// generated on 2018-10-18 using generator-webapp 3.0.1
const gulp = require('gulp');
const gulpLoadPlugins = require('gulp-load-plugins');
const browserSync = require('browser-sync').create();
const del = require('del');
// const wiredep = require('wiredep').stream;
const runSequence = require('run-sequence');

const $ = gulpLoadPlugins();
const reload = browserSync.reload;

let dev = true;

gulp.task('styles', () => {
  return gulp.src('app/styles/*.css')
    //.pipe($.if(dev, $.sourcemaps.init()))
    .pipe($.autoprefixer({ browsers: ['> 1%', 'last 2 versions', 'Firefox ESR'] }))
    //.pipe($.if(dev, $.sourcemaps.write()))
    .pipe(gulp.dest('.tmp/styles'))
    // .pipe(gulp.dest('tessst'))
    .pipe(reload({ stream: true }));
});

// do nothing for now
gulp.task('copyCSS', function () {
  return gulp.src('.tmp/styles/*.css').pipe(gulp.dest('../Site/Chromascape/styles'));
});
gulp.task('copyJS', function () {
  return gulp.src('.tmp/scripts/*.js').pipe(gulp.dest('../Site/Chromascape/scripts'));
});

gulp.task('scripts', () => {
  return gulp.src('app/scripts/**/*.js')
    .pipe($.plumber())
    .pipe($.if(dev, $.sourcemaps.init()))
    .pipe($.babel())
    .pipe($.if(dev, $.sourcemaps.write('.')))
    .pipe(gulp.dest('.tmp/scripts'))
    .pipe(reload({ stream: true }));
});

gulp.task('html', ['styles', 'views', 'scripts'], () => {
  return gulp.src('app/*.html')
    .pipe($.useref({ searchPath: ['.tmp', 'app', '.'] }))
    // .pipe($.if(/\.js$/, $.uglify({ compress: { drop_console: true } })))
    // .pipe($.if(/\.css$/, $.cssnano({ safe: true, autoprefixer: false })))
    .pipe($.if(/\.html$/, $.htmlmin({
      // collapseWhitespace: true,
      // minifyCSS: false,
      // minifyJS: { compress: { drop_console: true } },
      // processConditionalComments: true,
      // removeComments: true,
      // removeEmptyAttributes: true,
      // removeScriptTypeAttributes: true,
      // removeStyleLinkTypeAttributes: true
    })))
    .pipe(gulp.dest('dist'));
});

gulp.task('views', () => {
  return gulp.src('app/pages/*.njk')
    .pipe($.nunjucksRender({
      path: 'app'
    }))
    .pipe(gulp.dest('app/'))
    .pipe(reload({ stream: true }));
});

gulp.task('views:reload', ['views'], () => {
  reload();
});

gulp.task('images', () => {
  return gulp.src('app/images/**/*')
    .pipe($.cache($.imagemin()))
    .pipe(gulp.dest('dist/images'));
});

gulp.task('extras', () => {
  return gulp.src([
    'app/*',
    '!app/*.html'
  ], {
      dot: true
    }).pipe(gulp.dest('dist'));
});

gulp.task('clean', del.bind(null, ['.tmp', 'dist']));

// The main task to start the local server
gulp.task('serve', () => {
  runSequence(['clean'], ['styles', 'scripts'], () => {
    browserSync.init({
      notify: false,
      port: 9000,
      server: {
        baseDir: ['.tmp', 'app'],
        routes: {
          '/bower_components': 'bower_components'
        }
      }
    });

    gulp.watch([
      'app/*.html',
      'app/images/**/*',
    ]).on('change', reload);

    gulp.watch('app/styles/**/*.css', ['styles', 'copyCSS']);
    gulp.watch('app/scripts/**/*.js', ['scripts', 'copyJS']);
    gulp.watch('app/**/*.{html,njk}', ['views:reload']);
  });
});

// Use top copy over CSS and JS to Kentico project
gulp.task('copy', ['copyCSS', 'copyJS',], () => {
  // return gulp.src('dist/**/*').pipe($.size({ title: 'build', gzip: true }));
});
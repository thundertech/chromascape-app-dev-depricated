﻿using Business.Dto.Leadership;
using System.Collections.Generic;

namespace Business.Interfaces
{
    public interface ILeadership
    {
        LeadershipContainerDto GetLeadershipContainer();

        List<LeadershipMemberDto> GetLeadershipTeam();
    }
}

﻿using Business.Dto.ProductBrand;
using System.Collections.Generic;

namespace Business.Interfaces
{
    public interface IProductBrand
    {
        List<ProductBrandDto> GetProductsBrands();
    }
}

﻿using Business.Dto.SiteFooter;

namespace Business.Interfaces
{
    public interface ISiteFooter
    {
        SiteFooterDto GetSiteFooter();
    }
}

﻿using Business.Dto.Industries;
using System.Collections.Generic;

namespace Business.Interfaces
{
    public interface IIndustries
    {
        List<IndustryDto> GetIndustries(string industryNodeAliasPath);
    }
}

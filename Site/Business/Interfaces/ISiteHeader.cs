﻿using Business.Dto.SiteHeader;

namespace Business.Interfaces
{
    public interface ISiteHeader
    {
        SiteHeaderDto GetSiteHeader();
    }
}

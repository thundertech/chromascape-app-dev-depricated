﻿using Business.Dto.Markets;
using Business.Repository;
using System.Collections.Generic;

namespace Business.Interfaces
{
    public interface IMarkets : IRepository
    {
        List<MarketDto> GetMarkets();

        List<MarketLinkDto> GetMarketLinks(string nodeAliasPath);
    }
}

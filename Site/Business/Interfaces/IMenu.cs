﻿using Business.Dto.Menus;
using System.Collections.Generic;

namespace Business.Interfaces
{
    public interface IMenu
    {
        MenuFolderDto GetMenuFolder(string nodeAliasPath);

        List<MenuItemDto> GetMenuItems(string folderAliasPath, int nestingLevel);
    }
}

﻿using Business.Dto.Resources;
using System.Collections.Generic;

namespace Business.Interfaces
{
    public interface IResourceItem
    {
        List<ResourceItemDto> GetResources();
    }
}

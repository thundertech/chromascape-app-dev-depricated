﻿using Business.Dto.HeroCallout;

namespace Business.Interfaces
{
    public interface IHeroCallout
    {
        HeroCalloutDto GetHeroCallout(string nodeAliasPath);
    }
}

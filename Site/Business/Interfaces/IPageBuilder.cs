﻿using Business.Dto.PageBuilderPage;
using Business.Repository;

namespace Business.Interfaces
{
    public interface IPageBuilder : IRepository
    {
        PageBuilderDto GetPageBuilderPage(string pageAlias);
    }
}

﻿using Business.Dto.SocialLinks;
using Business.Repository;
using System.Collections.Generic;

namespace Business.Interfaces
{
    public interface ISocialLink : IRepository
    {
        List<SocialLinksDto> GetSocialLinks();
    }
}

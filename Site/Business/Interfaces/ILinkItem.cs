﻿using Business.Dto.LinkListing;
using System.Collections.Generic;

namespace Business.Interfaces
{
    public interface ILinkItem
    {
        List<LinkItemDto> GetLinks(string linkGroupNodeAliasPath);
    }
}

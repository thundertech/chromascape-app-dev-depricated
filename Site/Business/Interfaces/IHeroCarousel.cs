﻿using Business.Dto.HeroCarousel;
using Business.Repository;
using System.Collections.Generic;

namespace Business.Interfaces
{
    public interface IHeroCarousel : IRepository
    {
        List<HeroCarouselSlideDto> GetSlides(string parentNodeAliasPath);
    }
}

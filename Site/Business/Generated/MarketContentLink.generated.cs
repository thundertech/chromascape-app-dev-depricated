//--------------------------------------------------------------------------------------------------
// <auto-generated>
//
//     This code was generated by code generator tool.
//
//     To customize the code use your own partial class. For more info about how to use and customize
//     the generated code see the documentation at http://docs.kentico.com.
//
// </auto-generated>
//--------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

using CMS;
using CMS.Base;
using CMS.Helpers;
using CMS.DataEngine;
using CMS.DocumentEngine.Types.ChromaScape;
using CMS.DocumentEngine;

[assembly: RegisterDocumentType(MarketContentLink.CLASS_NAME, typeof(MarketContentLink))]

namespace CMS.DocumentEngine.Types.ChromaScape
{
	/// <summary>
	/// Represents a content item of type MarketContentLink.
	/// </summary>
	public partial class MarketContentLink : TreeNode
	{
		#region "Constants and variables"

		/// <summary>
		/// The name of the data class.
		/// </summary>
		public const string CLASS_NAME = "ChromaScape.MarketContentLink";


		/// <summary>
		/// The instance of the class that provides extended API for working with MarketContentLink fields.
		/// </summary>
		private readonly MarketContentLinkFields mFields;

		#endregion


		#region "Properties"

		/// <summary>
		/// MarketContentLinkID.
		/// </summary>
		[DatabaseIDField]
		public int MarketContentLinkID
		{
			get
			{
				return ValidationHelper.GetInteger(GetValue("MarketContentLinkID"), 0);
			}
			set
			{
				SetValue("MarketContentLinkID", value);
			}
		}


		/// <summary>
		/// Link Title (i.e. Mulch Colorant).
		/// </summary>
		[DatabaseField]
		public string LinkTitle
		{
			get
			{
				return ValidationHelper.GetString(GetValue("LinkTitle"), @"");
			}
			set
			{
				SetValue("LinkTitle", value);
			}
		}


		/// <summary>
		/// Link Url.
		/// </summary>
		[DatabaseField]
		public string LinkUrl
		{
			get
			{
				return ValidationHelper.GetString(GetValue("LinkUrl"), @"");
			}
			set
			{
				SetValue("LinkUrl", value);
			}
		}


		/// <summary>
		/// Gets an object that provides extended API for working with MarketContentLink fields.
		/// </summary>
		[RegisterProperty]
		public MarketContentLinkFields Fields
		{
			get
			{
				return mFields;
			}
		}


		/// <summary>
		/// Provides extended API for working with MarketContentLink fields.
		/// </summary>
		[RegisterAllProperties]
		public partial class MarketContentLinkFields : AbstractHierarchicalObject<MarketContentLinkFields>
		{
			/// <summary>
			/// The content item of type MarketContentLink that is a target of the extended API.
			/// </summary>
			private readonly MarketContentLink mInstance;


			/// <summary>
			/// Initializes a new instance of the <see cref="MarketContentLinkFields" /> class with the specified content item of type MarketContentLink.
			/// </summary>
			/// <param name="instance">The content item of type MarketContentLink that is a target of the extended API.</param>
			public MarketContentLinkFields(MarketContentLink instance)
			{
				mInstance = instance;
			}


			/// <summary>
			/// MarketContentLinkID.
			/// </summary>
			public int ID
			{
				get
				{
					return mInstance.MarketContentLinkID;
				}
				set
				{
					mInstance.MarketContentLinkID = value;
				}
			}


			/// <summary>
			/// Link Title (i.e. Mulch Colorant).
			/// </summary>
			public string LinkTitle
			{
				get
				{
					return mInstance.LinkTitle;
				}
				set
				{
					mInstance.LinkTitle = value;
				}
			}


			/// <summary>
			/// Link Url.
			/// </summary>
			public string LinkUrl
			{
				get
				{
					return mInstance.LinkUrl;
				}
				set
				{
					mInstance.LinkUrl = value;
				}
			}
		}

		#endregion


		#region "Constructors"

		/// <summary>
		/// Initializes a new instance of the <see cref="MarketContentLink" /> class.
		/// </summary>
		public MarketContentLink() : base(CLASS_NAME)
		{
			mFields = new MarketContentLinkFields(this);
		}

		#endregion
	}
}
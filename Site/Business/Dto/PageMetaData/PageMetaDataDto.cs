﻿namespace Business.Dto.PageMetaData
{
    public class PageMetaDataDto
    {
        public int DocumentId { get; set; }

        /// <summary>
        /// Page meta title
        /// </summary>
        public string PageTitle { get; set; }

        /// <summary>
        /// Page meta description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Page meta keywords
        /// </summary>
        public string Keywords { get; set; }

    }
}

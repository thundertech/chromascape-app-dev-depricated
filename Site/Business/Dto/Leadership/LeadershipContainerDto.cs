﻿namespace Business.Dto.Leadership
{
    public class LeadershipContainerDto
    {
        public string ContainerName { get; set; }
    }
}

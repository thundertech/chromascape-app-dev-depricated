﻿using CMS.DocumentEngine;

namespace Business.Dto.Leadership
{
    public class LeadershipMemberDto
    {
        public string Name { get; set; }

        public string JobTitle { get; set; }

        public DocumentAttachment Image { get; set; }
    }
}

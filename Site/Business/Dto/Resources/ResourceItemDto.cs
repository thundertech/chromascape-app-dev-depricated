﻿using CMS.DocumentEngine;

namespace Business.Dto.Resources
{
    public class ResourceItemDto
    {
        public string ResourceTitle { get; set; }

        public string ResourceDescription { get; set; }

        public DocumentAttachment ResourceFile { get; set; }

        public string ResourceIcon { get; set; }
        public string ResourceExternalLink { get; set; }
    }
}

﻿namespace Business.Dto.SiteFooter
{
    public class SiteFooterDto
    {
        public string ContactUsHeader { get; set; }

        public string ContactUsContent { get; set; }

        public string FollowUsHeader { get; set; }

        public string CTAButtonText { get; set; }

        public string CTAButtonUrl { get; set; }

        public string FooterNavFolderUrl { get; set; }
    }
}

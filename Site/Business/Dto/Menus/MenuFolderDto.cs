﻿using System.Collections.Generic;

namespace Business.Dto.Menus
{
    public class MenuFolderDto
    {
        public string Name { get; set; }

        public string NodeAliasPath { get; set; }

        public List<MenuItemDto> MenuItems { get; set; }
    }
}

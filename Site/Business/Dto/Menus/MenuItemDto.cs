﻿using System.Collections.Generic;

namespace Business.Dto.Menus
{
    public class MenuItemDto
    {
        public MenuItemDto()
        {
            SubItems = new List<MenuItemDto>();
        }

        public string Name { get; set; }

        public string Url { get; set; }

        public string NodeAliasPath { get; set; }

        public bool LinkTarget { get; set; }

        public List<MenuItemDto> SubItems { get; set; }

    }
}

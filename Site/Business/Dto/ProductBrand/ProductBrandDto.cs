﻿using CMS.DocumentEngine;

namespace Business.Dto.ProductBrand
{
    public class ProductBrandDto
    {
        public string Name { get; set; }

        public string Content { get; set; }

        public DocumentAttachment Image { get; set; }

        public string LinkText { get; set; }

        public string LinkUrl { get; set; }
    }
}

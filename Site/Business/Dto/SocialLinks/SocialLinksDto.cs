﻿namespace Business.Dto.SocialLinks
{
    public class SocialLinksDto
    {
        public string Title { get; set; }

        public string Url { get; set; }

        public string Icon { get; set; }
    }
}

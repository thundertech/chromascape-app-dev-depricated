﻿using CMS.DocumentEngine;

namespace Business.Dto.HeroCallout
{
    public class HeroCalloutDto
    {
        public string Title { get; set; }

        public string Content { get; set; }

        public DocumentAttachment Image { get; set; }

        public string ButtonText { get; set; }

        public string ButtonUrl { get; set; }
    }
}

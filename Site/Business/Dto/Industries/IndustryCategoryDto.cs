﻿using System.Collections.Generic;

namespace Business.Dto.Industries
{
    public class IndustryCategoryDto
    {
        public IndustryCategoryDto()
        {
            Industries = new List<IndustryDto>();
        }

        public string Name { get; set; }

        public List<IndustryDto> Industries { get; set; }
    }
}

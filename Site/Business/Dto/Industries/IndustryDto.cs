﻿using CMS.DocumentEngine;

namespace Business.Dto.Industries
{
    public class IndustryDto
    {
        public IndustryDto()
        {
            Image = new DocumentAttachment();
        }

        public string Name { get; set; }

        public string Url { get; set; }

        public DocumentAttachment Image { get; set; }
    }
}

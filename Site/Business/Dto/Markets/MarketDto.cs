﻿using CMS.DocumentEngine;
using System.Collections.Generic;

namespace Business.Dto.Markets
{
    public class MarketDto
    {
        public MarketDto()
        {
            MarketLinks = new List<MarketLinkDto>();
        }

        public string NodeAliasPath { get; set; }

        public string Title { get; set; }

        public string TitleUrl { get; set; }

        public string Subtitle { get; set; }

        public DocumentAttachment Image { get; set; }

        /// <summary>
        /// Determines if the image is on the left or the right
        /// </summary>
        public string ImageOption { get; set; }

        /// <summary>
        /// Determines the background color of the content block. Current options are Marigold and Cranberry
        /// </summary>
        public string ContentBackgroundColor { get; set; }

        public List<MarketLinkDto> MarketLinks { get; set; }
    }
}

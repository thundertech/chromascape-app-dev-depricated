﻿namespace Business.Dto.Markets
{
    public class MarketLinkDto
    {
        public string LinkTitle { get; set; }

        public string LinkUrl { get; set; }
    }
}

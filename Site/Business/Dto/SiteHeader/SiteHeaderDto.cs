﻿using CMS.DocumentEngine;

namespace Business.Dto.SiteHeader
{
    public class SiteHeaderDto
    {
        public DocumentAttachment LogoImage { get; set; }

        public string ContactUsText { get; set; }

        public string ContactUsUrl { get; set; }

        public string PhoneNumberText { get; set; }

        public string CTAButtonText { get; set; }

        public string CTAButtonUrl { get; set; }

        public string MainNavFolderUrl { get; set; }
    }
}

﻿using CMS.DocumentEngine;

namespace Business.Dto.HeroCarousel
{
    public class HeroCarouselSlideDto
    {
        public string Title { get; set; }

        public string SlideContent { get; set; }

        public DocumentAttachment BackgroundImage { get; set; }

        public string ButtonText { get; set; }

        public string ButtonUrl { get; set; }
    }
}

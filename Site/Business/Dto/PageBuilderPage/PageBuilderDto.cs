﻿namespace Business.Dto.PageBuilderPage
{
    public class PageBuilderDto
    {
        public int DocumentId { get; set; }

        public string Title { get; set; }

        public string NodeAliasPath { get; set; }

    }
}

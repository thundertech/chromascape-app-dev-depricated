﻿namespace Business.Dto.LinkListing
{
    public class LinkItemDto
    {
        public string Name { get; set; }

        public string LinkText { get; set; }

        public string Icon { get; set; }

        public string Url { get; set; }
    }
}

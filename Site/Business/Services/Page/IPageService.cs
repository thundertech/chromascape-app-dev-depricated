﻿using Business.Dto.PageMetaData;

namespace Business.Services.Page
{
    public interface IPageService
    {
        PageMetaDataDto GetHomePageMetaData(string pageAlias);

        PageMetaDataDto GetPageBuilderPageMetaData(string pageAlias);

    }
}

﻿using Business.Dto.PageMetaData;
using Business.Repository;
using Business.Services.Context;
using Business.Services.Query;
using System.Linq;

namespace Business.Services.Page
{
    public class PageService : BaseRepository, IPageService
    {

        private ISiteContextService SiteContextService { get; }

        public PageService(IDocumentQueryService documentQueryService, ISiteContextService siteContextService) : base(documentQueryService)
        {
            SiteContextService = siteContextService;
        }

        public PageMetaDataDto GetHomePageMetaData(string pageAlias)
        {
            var result = DocumentQueryService.GetDocuments<CMS.DocumentEngine.Types.ChromaScape.HomePage>()
                .AddColumns("DocumentID", "DocumentName", "DocumentPageTitle", "DocumentPageDescription", "DocumentPageKeywords")
                .Path(pageAlias)
                .OnCurrentSite()
                .TopN(1)
                .Select(pageBuilder => new PageMetaDataDto()
                {
                    DocumentId = pageBuilder.DocumentID,
                    PageTitle = pageBuilder.DocumentPageTitle,
                    Description = pageBuilder.DocumentPageDescription,
                    Keywords = pageBuilder.DocumentPageKeyWords
                }).FirstOrDefault();


            return result;
        }

        public PageMetaDataDto GetPageBuilderPageMetaData(string pageAlias)
        {
            var result = DocumentQueryService.GetDocuments<CMS.DocumentEngine.Types.ChromaScape.PageBuilderPage>()
                .AddColumns("DocumentID", "DocumentName", "DocumentPageTitle", "DocumentPageDescription", "DocumentPageKeywords")
                .Path(pageAlias)
                .OnCurrentSite()
                .TopN(1)
                .Select(pageBuilder => new PageMetaDataDto()
                {
                    DocumentId = pageBuilder.DocumentID,
                    PageTitle = pageBuilder.DocumentPageTitle,
                    Description = pageBuilder.DocumentPageDescription,
                    Keywords = pageBuilder.DocumentPageKeyWords
                }).FirstOrDefault();


            return result;
        }
    }
}

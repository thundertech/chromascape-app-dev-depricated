﻿using Business.Services.Context;
using CMS.DocumentEngine;
using System;
using System.Linq;

namespace Business.Services.Query
{
    public class DocumentQueryService : BaseService, IDocumentQueryService
    {
        private ISiteContextService SiteContext { get; }

        private readonly string[] _coreColumns =
        {
            "NodeGUID", "DocumentID", "NodeID"
        };

        public DocumentQueryService(ISiteContextService siteContext)
        {
            SiteContext = siteContext;
        }

        public DocumentQuery<TDocument> GetDocument<TDocument>(Guid nodeGuid) where TDocument : TreeNode, new()
        {
            return GetDocuments<TDocument>()
                .TopN(1)
                .WhereEquals("NodeGUID", nodeGuid);
        }

        public DocumentQuery<TDocument> GetDocuments<TDocument>() where TDocument : TreeNode, new()
        {
            var query = DocumentHelper.GetDocuments<TDocument>();

            // Evaluates whether preview mode is enabled
            if (SiteContext.IsPreviewEnabled)
            {
                // Loads the latest version of pages as preview mode is enabled
                query = query
                    .Columns(_coreColumns.Concat(new[] { "NodeSiteID" })) // Sets initial columns returned for optimization.
                                                                          //Adds 'NoteSiteD' column required for the Preview mode.
                    .OnSite(SiteContext.SiteName) // There could be more sites with matching page
                    .LatestVersion()
                    .Published(false)
                    .Culture(SiteContext.PreviewCulture);
            }

            if (!SiteContext.IsPreviewEnabled)
            {
                // Loads the published version of pages
                query = query
                    .Columns(_coreColumns) // Sets initial columns returned for optimization.
                    .OnSite(SiteContext.SiteName)
                    .Published()
                    .PublishedVersion()
                    .Culture(SiteContext.CurrentSiteCulture);
            }

            return query;
        }

        public DocumentQuery<TDocument> GetDocument<TDocument>(string pageAlias) where TDocument : TreeNode, new()
        {
            return GetDocuments<TDocument>()
                .TopN(1)
                .WhereEquals("NodeAlias", pageAlias);
        }

    }
}

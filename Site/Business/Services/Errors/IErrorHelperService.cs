﻿using System;

namespace Business.Services.Errors
{
    public interface IErrorHelperService : IService
    {
        void LogException(string source, string eventCode, Exception exception);
    }
}

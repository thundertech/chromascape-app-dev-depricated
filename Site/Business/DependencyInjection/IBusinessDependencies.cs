﻿using Business.Interfaces;
using Business.Services.Context;
using Business.Services.Page;

namespace Business.DependencyInjection
{
    public interface IBusinessDependencies
    {
        ISiteContextService SiteContextService { get; }

        IPageService PageService { get; }

        ISocialLink SocialLink { get; }

        IHeroCarousel HeroCarousel { get; }

        IMarkets Markets { get; }

        IPageBuilder PageBuilder { get; }

        ISiteFooter SiteFooter { get; }

        ISiteHeader SiteHeader { get; }

        IMenu Menu { get; }

        IProductBrand ProductBrand { get; }

        IHeroCallout HeroCallout { get; }

        ILeadership Leadership { get; }

        IIndustries Industries { get; }

        ILinkItem LinkItem { get; }

        IResourceItem ResourceItem { get; }
    }
}

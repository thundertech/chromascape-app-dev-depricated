﻿using Business.Interfaces;
using Business.Services.Context;
using Business.Services.Page;

namespace Business.DependencyInjection
{
    public class BusinessDependencies : IBusinessDependencies
    {
        public ISiteContextService SiteContextService { get; }

        public IPageService PageService { get; }

        public ISocialLink SocialLink { get; }

        public IHeroCarousel HeroCarousel { get; }

        public IMarkets Markets { get; }

        public IPageBuilder PageBuilder { get; }

        public ISiteFooter SiteFooter { get; }

        public ISiteHeader SiteHeader { get; }

        public IMenu Menu { get; }

        public IProductBrand ProductBrand { get; }

        public IHeroCallout HeroCallout { get; }

        public ILeadership Leadership { get; }

        public IIndustries Industries { get; }

        public ILinkItem LinkItem { get; }

        public IResourceItem ResourceItem { get; }

        public BusinessDependencies(
            ISiteContextService siteContextService,
            IPageService pageService,
            ISocialLink socialLink,
            IHeroCarousel heroCarousel,
            IMarkets markets,
            IPageBuilder pageBuilder,
            ISiteFooter siteFooter,
            ISiteHeader siteHeader,
            IMenu menu,
            IProductBrand productBrand,
            IHeroCallout heroCallout,
            ILeadership leadership,
            IIndustries industries,
            ILinkItem linkItem,
            IResourceItem resourceItem)
        {
            SiteContextService = siteContextService;
            PageService = pageService;
            SocialLink = socialLink;
            HeroCarousel = heroCarousel;
            Markets = markets;
            PageBuilder = pageBuilder;
            SiteFooter = siteFooter;
            SiteHeader = siteHeader;
            Menu = menu;
            ProductBrand = productBrand;
            HeroCallout = heroCallout;
            Leadership = leadership;
            Industries = industries;
            LinkItem = linkItem;
            ResourceItem = resourceItem;
        }

    }
}

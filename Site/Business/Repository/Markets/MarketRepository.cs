﻿using Business.Dto.Markets;
using Business.Interfaces;
using Business.Services.Context;
using Business.Services.Query;
using CMS.DocumentEngine.Types.ChromaScape;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Business.Repository.Markets
{
    public class MarketRepository : BaseRepository, IMarkets
    {
        public MarketRepository(IDocumentQueryService documentQueryService) : base(documentQueryService)
        {

        }

        private readonly string[] _marketColumns = {
            "NodeAliasPath", "MarketTitle", "TitleUrl", "Subtitle", "Image", "ImageOption", "ContentBackgroundColor"
        };

        private readonly string[] _marketLinkColumns = { "LinkTitle", "LinkUrl" };

        private Func<MarketContent, MarketDto> MarketContentDtoSelect => marketContent => new MarketDto()
        {
            NodeAliasPath = marketContent.NodeAliasPath,
            Title = marketContent.MarketTitle,
            TitleUrl = marketContent.TitleUrl,
            Subtitle = marketContent.Subtitle,
            Image = marketContent.Fields.Image,
            ImageOption = marketContent.Fields.ImageOption,
            ContentBackgroundColor = marketContent.Fields.ContentBackgroundColor
        };

        private Func<MarketContentLink, MarketLinkDto> MarketLinkDtoSelect => marketLink => new MarketLinkDto()
        {
            LinkTitle = marketLink.LinkTitle,
            LinkUrl = marketLink.LinkUrl
        };

        ISiteContextService SiteContextService { get; }

        public MarketRepository(IDocumentQueryService documentQueryService, ISiteContextService siteContextService) : base(documentQueryService)
        {
            SiteContextService = siteContextService;
        }

        public List<MarketDto> GetMarkets()
        {
            return DocumentQueryService.GetDocuments<MarketContent>()
                .AddColumns(_marketColumns)
                .Path("/Home", CMS.DocumentEngine.PathTypeEnum.Children).OrderBy("NodeOrder")
                .Select(MarketContentDtoSelect)
                .ToList();
        }

        public List<MarketLinkDto> GetMarketLinks(string nodeAliasPath)
        {
            return DocumentQueryService.GetDocuments<MarketContentLink>()
                .AddColumns(_marketLinkColumns)
                .Path(nodeAliasPath, CMS.DocumentEngine.PathTypeEnum.Children)
                .OrderBy("NodeOrder")
                .Select(MarketLinkDtoSelect)
                .ToList();
        }
    }
}

﻿using Business.Dto.LinkListing;
using Business.Interfaces;
using Business.Services.Context;
using Business.Services.Query;
using CMS.DocumentEngine.Types.ChromaScape;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Business.Repository.LinkItems
{
    public class LinkItemRepository : BaseRepository, ILinkItem
    {
        public LinkItemRepository(IDocumentQueryService documentQueryService) : base(documentQueryService)
        {

        }

        private string[] _linkItemColumns = new string[]
        {
            "LinkItemName", "LinkText", "LinkUrl", "LinkIcon"
        };

        private Func<LinkItem, LinkItemDto> LinkItemDtoSelect => linkItem => new LinkItemDto()
        {
            Name = linkItem.LinkItemName,
            LinkText = linkItem.LinkText,
            Url = linkItem.LinkUrl,
            Icon = linkItem.LinkIcon
        };

        ISiteContextService SiteContextService { get; }

        public LinkItemRepository(IDocumentQueryService documentQueryService, ISiteContextService siteContextService) : base(documentQueryService)
        {
            SiteContextService = siteContextService;
        }

        public List<LinkItemDto> GetLinks(string linkGroupNodeAliasPath)
        {
            return DocumentQueryService.GetDocuments<LinkItem>()
                .AddColumns(_linkItemColumns)
                .Path(linkGroupNodeAliasPath, CMS.DocumentEngine.PathTypeEnum.Children)
                .Select(LinkItemDtoSelect)
                .ToList();
        }
    }
}

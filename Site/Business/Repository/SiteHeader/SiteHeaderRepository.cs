﻿using Business.Dto.SiteHeader;
using Business.Interfaces;
using Business.Services.Context;
using Business.Services.Query;
using System;
using System.Linq;

namespace Business.Repository.SiteHeader
{
    public class SiteHeaderRepository : BaseRepository, ISiteHeader
    {
        public SiteHeaderRepository(IDocumentQueryService documentQueryService) : base(documentQueryService)
        {

        }

        private readonly string[] _siteHeaderColumns = { "Logo", "ContactUsText", "ContactUsUrl", "PhoneNumberText", "ButtonText", "ButtonUrl", "MainNavMenu" };

        private Func<CMS.DocumentEngine.Types.ChromaScape.SiteHeader, SiteHeaderDto> SiteHeaderDtoSelect => siteHeader => new SiteHeaderDto()
        {
            ContactUsText = siteHeader.ContactUsText,
            ContactUsUrl = siteHeader.ContactUsUrl,
            LogoImage = siteHeader.Fields.Logo,
            PhoneNumberText = siteHeader.PhoneNumberText,
            CTAButtonText = siteHeader.ButtonText,
            CTAButtonUrl = siteHeader.ButtonUrl,
            MainNavFolderUrl = siteHeader.MainNavMenu.Replace("~", "")
        };

        ISiteContextService SiteContextService { get; }


        public SiteHeaderRepository(IDocumentQueryService documentQueryService, ISiteContextService siteContextService) : base(documentQueryService)
        {
            SiteContextService = siteContextService;
        }

        public SiteHeaderDto GetSiteHeader()
        {
            return DocumentQueryService.GetDocuments<CMS.DocumentEngine.Types.ChromaScape.SiteHeader>()
                .AddColumns(_siteHeaderColumns)
                .Path("/Global", CMS.DocumentEngine.PathTypeEnum.Children)
                .Select(SiteHeaderDtoSelect)
                .FirstOrDefault();
        }
    }
}

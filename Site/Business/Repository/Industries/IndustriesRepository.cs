﻿using Business.Dto.Industries;
using Business.Interfaces;
using Business.Services.Context;
using Business.Services.Query;
using CMS.DocumentEngine.Types.ChromaScape;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Business.Repository.Industries
{
    public class IndustriesRepository : BaseRepository, IIndustries
    {
        public IndustriesRepository(IDocumentQueryService documentQueryService) : base(documentQueryService)
        {

        }

        private string[] _industryColumns = new string[]
        {
            "IndustryName", "IndustryImage", "IndustryUrl"
        };

        private Func<Industry, IndustryDto> IndustryDtoSelect => industry => new IndustryDto()
        {
            Name = industry.IndustryName,
            Image = industry.Fields.Image,
            Url = industry.IndustryUrl
        };

        ISiteContextService SiteContextService { get; }

        public IndustriesRepository(IDocumentQueryService documentQueryService, ISiteContextService siteContextService) : base(documentQueryService)
        {
            SiteContextService = siteContextService;
        }

        public List<IndustryDto> GetIndustries(string industryNodeAliasPath)
        {
            return DocumentQueryService.GetDocuments<Industry>()
                .AddColumns(_industryColumns)
                .Path(industryNodeAliasPath, CMS.DocumentEngine.PathTypeEnum.Children)
                .Select(IndustryDtoSelect)
                .ToList();
        }
    }
}

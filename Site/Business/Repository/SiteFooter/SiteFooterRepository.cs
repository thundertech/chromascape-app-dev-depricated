﻿using Business.Dto.SiteFooter;
using Business.Interfaces;
using Business.Services.Context;
using Business.Services.Query;
using System;
using System.Linq;

namespace Business.Repository.SiteFooter
{
    public class SiteFooterRepository : BaseRepository, ISiteFooter
    {
        public SiteFooterRepository(IDocumentQueryService documentQueryService) : base(documentQueryService)
        {

        }

        private readonly string[] _siteFooterColumns = { "ContactUsHeader", "ContactUsContent", "FooterNav", "FollowUsHeader", "ButtonText", "ButtonUrl" };

        private Func<CMS.DocumentEngine.Types.ChromaScape.SiteFooter, SiteFooterDto> SiteFooterDtoSelect => SiteFooter => new SiteFooterDto()
        {
            ContactUsHeader = SiteFooter.ContactUsHeader,
            ContactUsContent = SiteFooter.ContactUsContent,
            FollowUsHeader = SiteFooter.FollowUsHeader,
            CTAButtonText = SiteFooter.ButtonText,
            CTAButtonUrl = SiteFooter.ButtonUrl,
            FooterNavFolderUrl = SiteFooter.FooterNav.Replace("~", "")
        };

        ISiteContextService SiteContextService { get; }

        public SiteFooterRepository(IDocumentQueryService documentQueryService, ISiteContextService siteContextService) : base(documentQueryService)
        {
            SiteContextService = siteContextService;
        }

        public SiteFooterDto GetSiteFooter()
        {
            return DocumentQueryService.GetDocuments<CMS.DocumentEngine.Types.ChromaScape.SiteFooter>()
                .AddColumns(_siteFooterColumns)
                .Path("/Global", CMS.DocumentEngine.PathTypeEnum.Children)
                .Select(SiteFooterDtoSelect)
                .FirstOrDefault();
        }
    }
}

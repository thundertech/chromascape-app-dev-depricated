﻿using Business.Dto.ProductBrand;
using Business.Interfaces;
using Business.Services.Context;
using Business.Services.Query;
using CMS.DocumentEngine.Types.ChromaScape;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Business.Repository.ProductBrand
{
    public class ProductBrandRepository : BaseRepository, IProductBrand
    {
        public ProductBrandRepository(IDocumentQueryService documentQueryService) : base(documentQueryService)
        {

        }

        private readonly string[] _productBrandColumns = 
        {
            "ProductBrandName", "ProductBrandContent", "ProductBrandImage", "ProductBrandLinkText", "ProductBrandLinkURL"
        };

        private Func<ProductBrandItem, ProductBrandDto> ProductBrandDtoSelect => productBrand => new ProductBrandDto()
        {
            Name = productBrand.ProductBrandName,
            Content = productBrand.ProductBrandContent,
            Image = productBrand.Fields.ProductBrandImage,
            LinkText = productBrand.ProductBrandLinkText,
            LinkUrl = productBrand.ProductBrandLinkUrl
        };

        ISiteContextService SiteContextService { get; }

        public ProductBrandRepository(IDocumentQueryService documentQueryService, ISiteContextService siteContextService) : base(documentQueryService)
        {
            SiteContextService = siteContextService;
        }

        public List<ProductBrandDto> GetProductsBrands()
        {
            return DocumentQueryService.GetDocuments<ProductBrandItem>()
                .AddColumns(_productBrandColumns)
                .Path("/Home", CMS.DocumentEngine.PathTypeEnum.Children).OrderBy("NodeOrder")
                .Select(ProductBrandDtoSelect)
                .ToList();
        }
    }
}

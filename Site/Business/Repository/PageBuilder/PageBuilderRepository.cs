﻿using Business.Dto.PageBuilderPage;
using Business.Interfaces;
using Business.Services.Query;
using System.Linq;

namespace Business.Repository.PageBuilder
{
    public class PageBuilderRepository : BaseRepository, IPageBuilder
    {
        public PageBuilderRepository(IDocumentQueryService documentQueryService) : base(documentQueryService)
        {

        }

        public PageBuilderDto GetPageBuilderPage(string pageAlias)
        {
            return DocumentQueryService.GetDocument<CMS.DocumentEngine.Types.ChromaScape.PageBuilderPage>(pageAlias)
                .AddColumns("DocumentID", "DocumentName", "NodeAliasPath")
                .WhereEquals("NodeAlias", pageAlias)
                .ToList()
                .Select(pageBuilder => new PageBuilderDto()
                {
                    DocumentId = pageBuilder.DocumentID,
                    Title = pageBuilder.DocumentName,
                    NodeAliasPath = pageBuilder.NodeAliasPath
                }).FirstOrDefault();
        }
    }
}

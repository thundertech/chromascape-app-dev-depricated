﻿using Business.Dto.Menus;
using Business.Interfaces;
using Business.Services.Context;
using Business.Services.Query;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Business.Repository.Menu
{
    public class MenuRepository : BaseRepository, IMenu
    {
        public MenuRepository(IDocumentQueryService documentQueryService) : base(documentQueryService)
        {

        }

        private readonly string[] _menuFolderColumns =
        {
            "MenuFolderName", "NodeAliasPath"
        };

        private readonly string[] _menuItemColumns =
        {
            "MenuItemName", "MenuItemUrl", "NodeAliasPath", "LinkTarget"
        };

        private Func<CMS.DocumentEngine.Types.ChromaScape.MenuFolder, MenuFolderDto> MenuFolderDtoSelect => menuFolder => new MenuFolderDto()
        {
            Name = menuFolder.MenuFolderName,
            NodeAliasPath = menuFolder.NodeAliasPath,
            MenuItems = GetMenuItems(menuFolder.NodeAliasPath, 1)
        };

        private Func<CMS.DocumentEngine.Types.ChromaScape.MenuContainerItem, MenuItemDto> MenuItemDtoSelect => menuItem => new MenuItemDto()
        {
            Name = menuItem.MenuItemName,
            Url = menuItem.MenuItemUrl,
            NodeAliasPath = menuItem.NodeAliasPath,
            LinkTarget = menuItem.LinkTarget
        };

        ISiteContextService SiteContextService { get; }

        public MenuRepository(IDocumentQueryService documentQueryService, ISiteContextService siteContextService) : base(documentQueryService)
        {
            SiteContextService = siteContextService;
        }

        public MenuFolderDto GetMenuFolder(string nodeAliasPath)
        {
            return DocumentQueryService.GetDocuments<CMS.DocumentEngine.Types.ChromaScape.MenuFolder>()
                .Path(nodeAliasPath)
                .AddColumns(_menuFolderColumns)
                .Select(MenuFolderDtoSelect)
                .FirstOrDefault();
        }

        public List<MenuItemDto> GetMenuItems(string folderAliasPath, int nestingLevel)
        {
            return DocumentQueryService.GetDocuments<CMS.DocumentEngine.Types.ChromaScape.MenuContainerItem>()
                .Path(folderAliasPath, CMS.DocumentEngine.PathTypeEnum.Children)
                .OrderBy("NodeOrder")
                .NestingLevel(nestingLevel)
                .AddColumns(_menuItemColumns)
                .Select(MenuItemDtoSelect)
                .ToList();
        }
    }
}

﻿using Business.Dto.SocialLinks;
using Business.Interfaces;
using Business.Services.Query;
using System.Collections.Generic;
using System.Linq;

namespace Business.Repository.SocialLink
{
    public class SocialLinkRepository : BaseRepository, ISocialLink
    {
        public SocialLinkRepository(IDocumentQueryService docQueryService) : base(docQueryService)
        {

        }

        public List<SocialLinksDto> GetSocialLinks()
        {
            return DocumentQueryService.GetDocuments<CMS.DocumentEngine.Types.ChromaScape.SocialLink>()
                .AddColumns("Title", "Url", "Icon")
                .OrderByAscending("NodeOrder")
                .ToList()
                .Select(s => new SocialLinksDto()
                {
                    Title = s.Title,
                    Url = s.Url,
                    Icon = s.Icon1
                }).ToList();
        }
    }
}

﻿using Business.Dto.Resources;
using Business.Interfaces;
using Business.Services.Context;
using Business.Services.Query;
using CMS.DocumentEngine.Types.ChromaScape;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Business.Repository.Resources
{
    public class ResourceItemRepository : BaseRepository, IResourceItem
    {
        public ResourceItemRepository(IDocumentQueryService documentQueryService) : base(documentQueryService)
        {

        }


        private string[] _resourceItemColumns = new string[]
        {
            //TODO: add item order?
            "ResourceTitle", "ResourceDescription", "ResourceFile", "ResourceLinkIcon", "ResourceExternalLink"
        };

        private Func<ResourceLinkItem, ResourceItemDto> ResourceItemDtoSelect => resourceItem => new ResourceItemDto()
        {
            ResourceTitle = resourceItem.ResourceTitle,
            ResourceDescription = resourceItem.ResourceDescription,
            ResourceFile = resourceItem.Fields.ResourceFile,
            ResourceIcon = resourceItem.ResourceLinkIcon,
            ResourceExternalLink = resourceItem.ResourceExternalLink
        };

        public List<ResourceItemDto> GetResources()
        {
            return DocumentQueryService.GetDocuments<ResourceLinkItem>()
                .AddColumns(_resourceItemColumns)
                .Path("/Resources-(1)", CMS.DocumentEngine.PathTypeEnum.Children).OrderBy("NodeOrder")
                .Select(ResourceItemDtoSelect)
                .ToList();
        }



    }
}

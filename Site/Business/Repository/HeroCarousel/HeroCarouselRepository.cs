﻿using Business.Dto.HeroCarousel;
using Business.Interfaces;
using Business.Services.Context;
using Business.Services.Query;
using CMS.DocumentEngine.Types.ChromaScape;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Business.Repository.HeroCarousel
{
    public class HeroCarouselRepository : BaseRepository, IHeroCarousel
    {
        public HeroCarouselRepository(IDocumentQueryService documentQueryService) : base(documentQueryService)
        {

        }

        private readonly string[] _heroSlideColumns = {
            "NodeAliasPath", "Title", "SlideContent", "BackgroundImage", "ButtonText", "ButtonUrl"
        };

        private Func<HeroCarouselSlide, HeroCarouselSlideDto> HeroSlideDtoSelect => heroSlide => new HeroCarouselSlideDto()
        {
            Title = heroSlide.Title,
            SlideContent = heroSlide.SlideContent,
            BackgroundImage = heroSlide.Fields.BackgroundImage,
            ButtonText = heroSlide.ButtonText,
            ButtonUrl = heroSlide.ButtonUrl
        };

        ISiteContextService SiteContextService { get; }

        public HeroCarouselRepository(IDocumentQueryService documentQueryService, ISiteContextService siteContextService) : base(documentQueryService)
        {
            SiteContextService = siteContextService;
        }

        public List<HeroCarouselSlideDto> GetSlides(string parentNodeAliasPath)
        {
            return DocumentQueryService.GetDocuments<HeroCarouselSlide>()
                .AddColumns(_heroSlideColumns)
                .Path(parentNodeAliasPath, CMS.DocumentEngine.PathTypeEnum.Children).OrderBy("NodeOrder")
                .Select(HeroSlideDtoSelect)
                .ToList();
        }
    }
}

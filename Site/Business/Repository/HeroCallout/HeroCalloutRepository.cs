﻿using Business.Dto.HeroCallout;
using Business.Interfaces;
using Business.Services.Context;
using Business.Services.Query;
using System;
using System.Linq;

namespace Business.Repository.HeroCallout
{
    public class HeroCalloutRepository : BaseRepository, IHeroCallout
    {
        public HeroCalloutRepository(IDocumentQueryService documentQueryService) : base(documentQueryService)
        {

        }

        private string[] _heroCalloutColumns = new string[]
        {
            "Title", "Content", "Image", "ButtonText", "ButtonUrl"
        };

        private Func<CMS.DocumentEngine.Types.ChromaScape.HeroCallout, HeroCalloutDto> HeroCalloutDtoSelect => heroCallout => new HeroCalloutDto()
        {
            Title = heroCallout.Title,
            Content = heroCallout.Content,
            ButtonText = heroCallout.ButtonText,
            ButtonUrl = heroCallout.ButtonUrl,
            Image = heroCallout.Fields.Image
        };

        ISiteContextService SiteContextService { get; }

        public HeroCalloutRepository(IDocumentQueryService documentQueryService, ISiteContextService siteContextService) : base(documentQueryService)
        {
            SiteContextService = siteContextService;
        }

        public HeroCalloutDto GetHeroCallout(string nodeAliasPath)
        {
            return DocumentQueryService.GetDocuments<CMS.DocumentEngine.Types.ChromaScape.HeroCallout>()
                .AddColumns(_heroCalloutColumns)
                .Path(nodeAliasPath, CMS.DocumentEngine.PathTypeEnum.Children)
                .Select(HeroCalloutDtoSelect)
                .FirstOrDefault();
        }
    }
}

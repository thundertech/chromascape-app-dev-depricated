﻿using Business.Dto.Leadership;
using Business.Interfaces;
using Business.Services.Context;
using Business.Services.Query;
using CMS.DocumentEngine.Types.ChromaScape;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Business.Repository.Leadership
{
    public class LeadershipRepository : BaseRepository, ILeadership
    {
        public LeadershipRepository(IDocumentQueryService documentQueryService) : base(documentQueryService)
        {

        }

        private string[] _leadershipMemberColumns = new string[]
        {
            "MemberName", "JobTitle", "Image"
        };

        private Func<LeadershipMember, LeadershipMemberDto> LeadershipMemberDtoSelect => leadershipMember => new LeadershipMemberDto()
        {
            Name = leadershipMember.MemberName,
            JobTitle = leadershipMember.JobTitle,
            Image = leadershipMember.Fields.Image
        };

        ISiteContextService SiteContextService { get; }

        public LeadershipRepository(IDocumentQueryService documentQueryService, ISiteContextService siteContextService) : base(documentQueryService)
        {
            SiteContextService = siteContextService;
        }

        public LeadershipContainerDto GetLeadershipContainer()
        {
            throw new NotImplementedException();
        }

        public List<LeadershipMemberDto> GetLeadershipTeam()
        {
            return DocumentQueryService.GetDocuments<LeadershipMember>()
                .AddColumns(_leadershipMemberColumns)
                .Path("/Leadership", CMS.DocumentEngine.PathTypeEnum.Children).OrderBy("NodeOrder")
                .Select(LeadershipMemberDtoSelect)
                .ToList();
        }
    }
}

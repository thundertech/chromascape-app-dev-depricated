using Kentico.Activities.Web.Mvc;
using Kentico.CampaignLogging.Web.Mvc;
using Kentico.Content.Web.Mvc;
using Kentico.OnlineMarketing.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

namespace Chromascape
{
    public class ApplicationConfig
    {
        public static void RegisterFeatures(IApplicationBuilder builder)
        {

            builder.UsePreview();
            builder.UsePageBuilder(new PageBuilderOptions()
            {
                DefaultSectionIdentifier = "ChromaScape.Sections.DefaultSection",
                RegisterDefaultSection = false
            });
        }
    }
}
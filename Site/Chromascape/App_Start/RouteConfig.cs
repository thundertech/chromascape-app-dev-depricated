using Kentico.Web.Mvc;
using System.Web.Mvc;
using System.Web.Routing;

namespace Chromascape
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.LowercaseUrls = true;

            // Maps routes to Kentico HTTP handlers and features enabled in ApplicationConfig.cs
            // Always map the Kentico routes before adding other routes. Issues may occur if Kentico URLs are matched by a general route, for example images might not be displayed on pages
            routes.Kentico().MapRoutes();

            // Maps route to landing pages
            routes.MapRoute(
                name: "PageBuilder",
                url: "{nodeAlias}",
                defaults: new { controller = "PageBuilder", action = "Index" }
            );

            routes.MapRoute(
                name: "News",
                url: "News/{nodeAlias}",
                defaults: new { controller = "PageBuilder", action = "Index" }
            );

            routes.MapRoute(
                name: "Markets",
                url: "markets/{nodeAlias}",
                defaults: new { controller = "PageBuilder", action = "Index" }
            );

            //routes.MapRoute(
              //  name: "Markets-Landscape",
              //  url: "marketslandscape/{nodeAlias}",
              //  defaults: new { controller = "PageBuilder", action = "Index" }
           // );

            routes.MapRoute(
                name: "Landscape",
                url: "landscape/{nodeAlias}",
                defaults: new { controller = "PageBuilder", action = "Index" }
            );

          //  routes.MapRoute(
              //  name: "Markets-Pigment-Dispersions",
              //  url: "markets/pigment-dispersions/{nodeAlias}",
               // defaults: new { controller = "PageBuilder", action = "Index" }
           // );

            routes.MapRoute(
                name: "Pigment-Dispersions",
                url: "pigment-dispersions/{nodeAlias}",
                defaults: new { controller = "PageBuilder", action = "Index" }
            );

            routes.MapRoute(
                name: "Resources",
                url: "Resources/{nodeAlias}",
                defaults: new { controller = "Resources", action = "Index" }
            );

            routes.MapRoute(
                name: "Insights",
                url: "Insights/{nodeAlias}",
                defaults: new { controller = "Insights", action = "Index" }
            );

            routes.MapRoute(
                name: "Who-We-Are",
                url: "Who-We-Are/{nodeAlias}",
                defaults: new { controller = "PageBuilder", action = "Index" }
            );

            routes.MapRoute(
                name: "Products",
                url: "Products/{nodeAlias}",
                defaults: new { controller = "PageBuilder", action = "Index" }
            );

            routes.MapRoute(
                name: "HomePage",
                url: "Home",
                defaults: new { controller = "Home", action = "Index" }
            );

            routes.MapRoute(
                name: "Unsubscribe",
                url: "Subscriptions/Unsubscribe",
                defaults: new { controller = "Subscriptions", action = "Unsubscribe" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}

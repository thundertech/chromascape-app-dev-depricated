using Chromascape.App_Start;
using Kentico.Activities.Web.Mvc;
using Kentico.CampaignLogging.Web.Mvc;
using Kentico.Content.Web.Mvc.Routing;
using Kentico.Newsletters.Web.Mvc;
using Kentico.OnlineMarketing.Web.Mvc;
using Kentico.Web.Mvc;
using System.Web.Routing;

namespace Chromascape
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            // Gets the ApplicationBuilder instance
            // Allows you to enable and configure selected Kentico MVC integration features
            ApplicationBuilder builder = ApplicationBuilder.Current;

            // Enables the email tracking feature, registers the following routes by default:
            // "CMSModules/Newsletters/CMSPages/Track.ashx" route for opened marketing email tracking
            // "CMSModules/Newsletters/CMSPages/Redirect.ashx" route for clicked link tracking
            builder.UseEmailTracking(new EmailTrackingOptions());

            // Enables the activity tracking feature
            builder.UseActivityTracking();
            builder.UseCampaignLogger();
            builder.UseActivityTracking();
            builder.UseABTesting();

            builder.UsePageRouting(new PageRoutingOptions
            {
                EnableAlternativeUrls = true
            });

            // Enables and configures selected Kentico ASP.NET MVC integration features
            ApplicationConfig.RegisterFeatures(builder);


            // Registers routes including system routes for enabled features
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            // Dependency injection
            AutoFacConfig.ConfigureContainer();

        }
    }
}

﻿
namespace Chromascape.Config
{
    public class AppConfig
    {
        public const string Sitename = "ChromaScape";

        public const string HomePagePath = "/Home";
    }
}
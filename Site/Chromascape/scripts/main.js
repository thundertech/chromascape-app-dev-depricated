'use strict';

$(document).ready(function () {

  $('.header-search-icon').on('click', function () {
    $('.header-search:first').toggleClass('is-active');
    $('.header-search input').focus();
  });

  $('.carousel').slick({
    dots: true
  });
});

$('#mobile-nav-toggle').on('click', function () {
  $(this).parents('.header-bottom').toggleClass('is-active');
});

$('.mobile-nav-children-toggle').on('click', function () {
  $(this).parent().toggleClass('is-active');
});
//# sourceMappingURL=main.js.map

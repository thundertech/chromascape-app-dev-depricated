﻿using Business.DependencyInjection;
using Business.Dto.Menus;
using Business.Dto.PageMetaData;
using Business.Dto.SiteFooter;
using Business.Dto.SiteHeader;
using Business.Dto.SocialLinks;
using Chromascape.Config;
using Chromascape.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Chromascape.Controllers
{
    public class BaseController : Controller
    {
        protected IBusinessDependencies Dependencies { get; set; }

        public BaseController(IBusinessDependencies busDeps)
        {
            Dependencies = busDeps;
        }

        public PageViewModel GetPageViewModel(string nodeAliasPath)
        {
            PageViewModel model = new PageViewModel();

            model.Metadata = GetPageMetadata(nodeAliasPath);
            model.SocialLinks = GetSocialLinks();
            model.SiteFooter = GetSiteFooter();
            model.SiteHeader = GetSiteHeader();
            //Get Main Nav
            GetMainNav(model);

            if (nodeAliasPath == "/Home")
                model.ShowBreadcrumbs = false;

            if (nodeAliasPath != "/Home")
                GetBreadcrumbs(nodeAliasPath, model);

            //Get footer nav
            if (model.SiteFooter != null)
                model.FooterMenuFolder.MenuItems = GetMenuItems(model.SiteFooter.FooterNavFolderUrl, 1);

            return model;
        }

        private static void GetBreadcrumbs(string nodeAliasPath, PageViewModel model)
        {
            var urlPieces = nodeAliasPath.Split('/').Where(u => !string.IsNullOrEmpty(u)).ToList();

            if (urlPieces != null && urlPieces.Count > 0)
            {
                foreach (var piece in urlPieces)
                {
                    Models.Local.BreadcrumbLink breadcrumb = new Models.Local.BreadcrumbLink();

                    breadcrumb = BuildBreadcrumb(piece);

                    if (piece == urlPieces.Last().ToString())
                    {
                        breadcrumb.IsActive = true;
                        breadcrumb.Url = nodeAliasPath;
                    }

                    model.Breadcrumbs.Add(breadcrumb);
                }
            }
        }

        private static Models.Local.BreadcrumbLink BuildBreadcrumb(string piece)
        {
            Models.Local.BreadcrumbLink breadcrumb = new Models.Local.BreadcrumbLink();

            breadcrumb.Name = piece.Replace("-", " ");
            breadcrumb.Url = "~/" + piece;
            breadcrumb.IsActive = false;

            return breadcrumb;
        }

        private void GetMainNav(PageViewModel model)
        {
            if (model.SiteHeader != null)
            {
                model.MainMenuFolder = GetMenuFolder(model.SiteHeader.MainNavFolderUrl);

                foreach (var menuItem in model.MainMenuFolder.MenuItems)
                {
                    var subItems = GetMenuItems(menuItem.NodeAliasPath, 1);

                    if (subItems != null)
                        menuItem.SubItems.AddRange(subItems);

                    foreach (var subItem in subItems)
                    {
                        var grandChildren = GetMenuItems(subItem.NodeAliasPath, 1);

                        if (grandChildren != null)
                            subItem.SubItems.AddRange(grandChildren);
                    }
                }
            }
        }

        private PageMetaDataDto GetPageMetadata(string nodeAliasPath)
        {
            if (nodeAliasPath == AppConfig.HomePagePath)
                return Dependencies.PageService.GetHomePageMetaData(nodeAliasPath);

            return Dependencies.PageService.GetPageBuilderPageMetaData(nodeAliasPath);
        }

        private List<SocialLinksDto> GetSocialLinks()
        {
            return Dependencies.SocialLink.GetSocialLinks();
        }

        private SiteFooterDto GetSiteFooter()
        {
            return Dependencies.SiteFooter.GetSiteFooter();
        }

        private SiteHeaderDto GetSiteHeader()
        {
            return Dependencies.SiteHeader.GetSiteHeader();
        }

        private MenuFolderDto GetMenuFolder(string folderAliasPath)
        {
            return Dependencies.Menu.GetMenuFolder(folderAliasPath);
        }

        private List<MenuItemDto> GetMenuItems(string folderAliasPath, int nestingLevel)
        {
            return Dependencies.Menu.GetMenuItems(folderAliasPath, nestingLevel);
        }
    }
}
﻿using Business.DependencyInjection;
using Chromascape.Models;
using Chromascape.Models.EmailMarketing;
using CMS.Newsletters;
using CMS.SiteProvider;
using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;
using System.Web.Mvc;

namespace Chromascape.Controllers.EmailMarketing
{
    public class SubscriptionsController : BaseController
    {

        private readonly IUnsubscriptionProvider _unsubscriptionProvider;
        private readonly IEmailHashValidator _emailHashValidatorService;
        private readonly ISubscriptionService _subscriptionService;
        private IBusinessDependencies _busDeps { get; }


        public SubscriptionsController(IUnsubscriptionProvider unsubscriptionProvider, IEmailHashValidator emailHashValidator, 
            ISubscriptionService subscriptionService, IBusinessDependencies businessDependencies) : base(businessDependencies)
        {
            _unsubscriptionProvider = unsubscriptionProvider;
            _emailHashValidatorService = emailHashValidator;
            _subscriptionService = subscriptionService;
        }

        /// <summary>
        /// Handles marketing email unsubscription requests.
        /// </summary>
        public ActionResult Unsubscribe(MarketingEmailUnsubscribeModel model)
        {
            PageViewModel pageViewModel = GetPageViewModel("/Home");

            // Verifies that the unsubscription request contains all required parameters
            if (ModelState.IsValid)
            {
                // Confirms whether the hash in the unsubscription request is valid for the given email address
                // Provides protection against forged unsubscription requests
                if (_emailHashValidatorService.ValidateEmailHash(model.Hash, model.Email))
                {
                    // Gets the marketing email (issue) from which the unsubscription request was sent
                    IssueInfo issue = IssueInfoProvider.GetIssueInfo(model.IssueGuid, SiteContext.CurrentSiteID);

                    if (model.UnsubscribeFromAll)
                    {
                        // Checks that the email address is not already unsubscribed from all marketing emails
                        if (!_unsubscriptionProvider.IsUnsubscribedFromAllNewsletters(model.Email))
                        {
                            // Unsubscribes the specified email address from all marketing emails (adds it to the opt-out list)
                            _subscriptionService.UnsubscribeFromAllNewsletters(model.Email, issue?.IssueID);
                        }
                    }
                    else
                    {
                        // Gets the email feed for which the unsubscription was requested
                        NewsletterInfo newsletter = NewsletterInfoProvider.GetNewsletterInfo(model.NewsletterGuid, SiteContext.CurrentSiteID);

                        if (newsletter != null)
                        {
                            // Checks that the email address is not already unsubscribed from the specified email feed
                            if (!_unsubscriptionProvider.IsUnsubscribedFromSingleNewsletter(model.Email, newsletter.NewsletterID))
                            {
                                // Unsubscribes the specified email address from the email feed
                                _subscriptionService.UnsubscribeFromSingleNewsletter(model.Email, newsletter.NewsletterID, issue?.IssueID);
                            }
                        }
                    }

                    // Displays a view to inform the user that they were unsubscribed
                    model.UnsubscribeSuccessful = true;
                }
            }

            // If the unsubscription was not successful, displays a view to inform the user
            // Failure can occur if the request does not provide all required parameters or if the hash is invalid
            model.UnsubscribeSuccessful = false;

            // Initializes the page builder with the DocumentID of the page
            HttpContext.Kentico().PageBuilder().Initialize(1);

            pageViewModel.UnsubscribeVM = model;

            return View(pageViewModel);
        }
    }
}
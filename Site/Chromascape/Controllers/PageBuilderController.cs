﻿using Business.DependencyInjection;
using Business.Interfaces;
using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;
using System.Web.Mvc;

namespace Chromascape.Controllers
{
    public class PageBuilderController : BaseController
    {
        protected IPageBuilder PageBuilder { get; }

        public PageBuilderController(IBusinessDependencies businessDependencies,
            IPageBuilder pageBuilder) : base(businessDependencies)
        {
            PageBuilder = pageBuilder;
        }

        // GET: PageBuilder
        public ActionResult Index(string nodeAlias)
        {
            var pageBuilderDto = PageBuilder.GetPageBuilderPage(nodeAlias);

            if (pageBuilderDto == null)
            {
                return HttpNotFound();
            }

            var model = GetPageViewModel(pageBuilderDto.NodeAliasPath);
            HttpContext.Kentico().PageBuilder().Initialize(pageBuilderDto.DocumentId);

            return View(model);
        }
    }
}
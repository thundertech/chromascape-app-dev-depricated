﻿using Business.DependencyInjection;
using Chromascape.Models;
using Chromascape.Models.Search;
using CMS.Membership;
using CMS.Search;
using System;
using System.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

namespace Chromascape.Controllers
{
    public class SearchController : BaseController
    {
        IBusinessDependencies _busDeps { get; }

        public SearchController(IBusinessDependencies businessDependencies) : base(businessDependencies)
        {
            _busDeps = businessDependencies;
        }

        // Adds the smart search indexes that will be used to perform searches
        public static readonly string[] searchIndexes = new string[] { "ChromaScapeSiteIndex" };
        // Sets the limit of items per page of search results
        private const int PAGE_SIZE = 10;

        public ActionResult SearchIndex(string searchText)
        {
            PageViewModel model = GetPageViewModel("/Home");

            model.SearchVM = new SearchViewModel();

            // Displays the search page without any search results if the query is empty
            if (String.IsNullOrWhiteSpace(searchText))
                return View(model);

            // Searches the specified index and gets the matching results
            SearchParameters searchParameters = SearchParameters.PrepareForPages(searchText, searchIndexes, 1, PAGE_SIZE, MembershipContext.AuthenticatedUser, "en-us", true);
            SearchResult searchResult = SearchHelper.Search(searchParameters);

            // Creates a model with the search result items
            model.SearchVM.Items = searchResult.Items;
            model.SearchVM.Query = searchText;

            // Initializes the page builder with the DocumentID of the page
            HttpContext.Kentico().PageBuilder().Initialize(1);

            return View(model);
        }
    }
}
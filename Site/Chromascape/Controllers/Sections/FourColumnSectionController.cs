﻿using Chromascape.Controllers.Sections;
using Chromascape.Models.Sections;
using Kentico.PageBuilder.Web.Mvc;
using System.Web.Mvc;

[assembly: RegisterSection("ChromaScape.Sections.FourColumn", typeof(FourColumnSectionController), "4 even columns", IconClass = "icon-l-cols-4")]
namespace Chromascape.Controllers.Sections
{
    public class FourColumnSectionController : SectionController<DefaultSectionProperties>
    {
        // GET: FourColumnSection
        public ActionResult Index()
        {
            var properties = GetProperties();
            DefaultSectionViewModel model = new DefaultSectionViewModel();

            model.BackgroundColor = properties.BackgroundColor;
            model.AddTopPadding = properties.AddPaddingTop;
            model.AddBottomPadding = properties.AddPaddingBottom;
            model.CustomCssClasses = properties.CustomCssClasses;

            return PartialView("Sections/_FourColumn", model);
        }
    }
}
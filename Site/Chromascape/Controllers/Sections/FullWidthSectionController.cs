﻿using Chromascape.Controllers.Sections;
using Chromascape.Models.Sections;
using Kentico.PageBuilder.Web.Mvc;
using System.Web.Mvc;

[assembly: RegisterSection("Chromascape.Sections.FullWidthSection", typeof(FullWidthSectionController), "Full Width Section", Description = "Section that covers entire width of the page.")]
namespace Chromascape.Controllers.Sections
{
    public class FullWidthSectionController : SectionController<DefaultSectionProperties>
    {
        public ActionResult Index()
        {
            var properties = GetProperties();
            DefaultSectionViewModel model = new DefaultSectionViewModel();

            model.BackgroundColor = properties.BackgroundColor;
            model.AddTopPadding = properties.AddPaddingTop;
            model.AddBottomPadding = properties.AddPaddingBottom;
            model.CustomCssClasses = properties.CustomCssClasses;

            return PartialView("Sections/_FullWidthSection", model);
        }
    }
}
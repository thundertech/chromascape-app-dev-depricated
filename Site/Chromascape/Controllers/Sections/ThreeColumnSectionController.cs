﻿using Chromascape.Controllers.Sections;
using Chromascape.Models.Sections;
using Kentico.PageBuilder.Web.Mvc;
using System.Web.Mvc;

[assembly: RegisterSection("ChromaScape.Sections.ThreeColumn", typeof(ThreeColumnSectionController), "3 even columns", IconClass = "icon-l-cols-3")]
namespace Chromascape.Controllers.Sections
{
    public class ThreeColumnSectionController : SectionController<DefaultSectionProperties>
    {
        // GET: ThreeColumnSection
        public ActionResult Index()
        {
            var properties = GetProperties();
            DefaultSectionViewModel model = new DefaultSectionViewModel();

            model.BackgroundColor = properties.BackgroundColor;
            model.AddTopPadding = properties.AddPaddingTop;
            model.AddBottomPadding = properties.AddPaddingBottom;
            model.CustomCssClasses = properties.CustomCssClasses;

            return PartialView("Sections/_ThreeColumn", model);
        }
    }
}
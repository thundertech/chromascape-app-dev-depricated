﻿using Chromascape.Controllers.Sections;
using Chromascape.Models.Sections;
using Kentico.PageBuilder.Web.Mvc;
using System.Web.Mvc;

[assembly: RegisterSection("ChromaScape.Sections.DefaultSection", typeof(DefaultSectionController), "1 column section", Description = "Single column layout", IconClass = "icon-l-cols-1")]
namespace Chromascape.Controllers.Sections
{
    public class DefaultSectionController : SectionController<DefaultSectionProperties>
    {
        public ActionResult Index()
        {
            DefaultSectionViewModel model = new DefaultSectionViewModel();
            var properties = GetProperties();

            model.BackgroundColor = properties.BackgroundColor;
            model.AddTopPadding = properties.AddPaddingTop;
            model.AddBottomPadding = properties.AddPaddingBottom;
            model.CustomCssClasses = properties.CustomCssClasses;

            return PartialView("Sections/_DefaultSection", model);
        }
    }
}
﻿using Chromascape.Controllers.Sections;
using Chromascape.Models.Sections;
using Kentico.PageBuilder.Web.Mvc;
using System.Web.Mvc;

[assembly: RegisterSection("ChromaScape.Sections.TwoColumn", typeof(TwoColumnSectionController), "2 split columns", IconClass = "icon-l-cols-2")]
namespace Chromascape.Controllers.Sections
{
    public class TwoColumnSectionController : SectionController<DefaultSectionProperties>
    {
        // GET: TwoColumnSection
        public ActionResult Index()
        {
            var properties = GetProperties();
            DefaultSectionViewModel model = new DefaultSectionViewModel();

            model.BackgroundColor = properties.BackgroundColor;
            model.AddTopPadding = properties.AddPaddingTop;
            model.AddBottomPadding = properties.AddPaddingBottom;
            model.CustomCssClasses = properties.CustomCssClasses;

            return PartialView("Sections/_TwoColumn", model);
        }
    }
}
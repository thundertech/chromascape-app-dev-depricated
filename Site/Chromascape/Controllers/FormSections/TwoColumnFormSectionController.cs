﻿using Chromascape.Controllers.FormSections;
using Kentico.Forms.Web.Mvc;
using System.Web.Mvc;

[assembly: RegisterFormSection("Chromascape.FormSections.TwoColumns", typeof(TwoColumnFormSectionController), 
    "Two columns", Description = "Organizes fields into two equal-width columns side-by-side.", IconClass = "icon-l-cols-2")]
namespace Chromascape.Controllers.FormSections
{
    public class TwoColumnFormSectionController : Controller
    {
        // GET: TwoColumnFormSection
        public ActionResult Index()
        {
            return PartialView("FormSections/_TwoColumnFormSection");
        }
    }
}
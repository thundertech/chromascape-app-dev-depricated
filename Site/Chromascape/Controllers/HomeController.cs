﻿using Business.DependencyInjection;
using Chromascape.Config;
using Chromascape.Models;
using CMS.DocumentEngine;
using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;
using System.Linq;
using System.Web.Mvc;

namespace Chromascape.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(IBusinessDependencies busDeps) : base(busDeps)
        {

        }

        public ActionResult Index()
        {
            // Retrieves the page from the Kentico database
            TreeNode page = DocumentHelper.GetDocuments()
                .Path(AppConfig.HomePagePath)
                .OnCurrentSite()
                .TopN(1)
                .FirstOrDefault();

            // Returns a 404 error when the retrieving is unsuccessful
            if (page == null)
            {
                return HttpNotFound();
            }

            // Initializes the page builder with the DocumentID of the page
            HttpContext.Kentico().PageBuilder().Initialize(page.DocumentID);

            PageViewModel model = GetPageViewModel(page.NodeAliasPath);

            return View(model);
        }
    }
}
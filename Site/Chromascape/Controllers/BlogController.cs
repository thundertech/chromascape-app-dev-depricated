﻿using Business.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Chromascape.Controllers
{
    public class BlogController : BaseController
    {
        public BlogController(IBusinessDependencies businessDependencies) : base(businessDependencies)
        {

        }
        // GET: Blog
        public ActionResult Index()
        {
            return View();
        }
    }
}
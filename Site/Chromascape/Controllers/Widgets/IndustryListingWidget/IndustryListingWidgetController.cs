﻿using Business.DependencyInjection;
using Chromascape.Controllers.Widgets.IndustryListingWidget;
using Chromascape.Models.Widgets.IndustryListingWidgets;
using Kentico.PageBuilder.Web.Mvc;
using System.Linq;
using System.Web.Mvc;

[assembly: RegisterWidget("Chromascape.Widgets.IndustryListing", typeof(IndustryListingWidgetController), "Industry Listing")]
namespace Chromascape.Controllers.Widgets.IndustryListingWidget
{
    public class IndustryListingWidgetController : WidgetController<IndustryListingWidgetProperties>
    {
        IBusinessDependencies _busDeps { get; }

        public IndustryListingWidgetController(IBusinessDependencies businessDependencies)
        {
            _busDeps = businessDependencies;
        }

        public ActionResult Index()
        {
            IndustryListingViewModel model = new IndustryListingViewModel();
            var properties = GetProperties();

            if (properties.IndustryCategoryPath != null)
            {
                model.IndustryCategoryPath = properties.IndustryCategoryPath.FirstOrDefault().NodeAliasPath;
                model.Industries = _busDeps.Industries.GetIndustries(model.IndustryCategoryPath);
            }

            model.CustomCssClasses = properties.CustomCssClasses;

            return PartialView("Widgets/_IndustryListingWidget", model);
        }
    }
}
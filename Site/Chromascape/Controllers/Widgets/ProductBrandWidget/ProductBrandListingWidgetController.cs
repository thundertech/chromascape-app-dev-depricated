﻿using Business.DependencyInjection;
using Chromascape.Controllers.Widgets.ProductBrandWidget;
using Chromascape.Models.Widgets.ProductBrandWidget;
using Kentico.PageBuilder.Web.Mvc;
using System.Web.Mvc;

[assembly: RegisterWidget("Chromascape.Widgets.ProductBrandWidget", typeof(ProductBrandListingWidgetController), "Products & Brands Services Listing Widget")]
namespace Chromascape.Controllers.Widgets.ProductBrandWidget
{
    public class ProductBrandListingWidgetController : WidgetController<ProductBrandWidgetProperties>
    {
        IBusinessDependencies _busDeps { get; }

        public ProductBrandListingWidgetController(IBusinessDependencies businessDependencies)
        {
            _busDeps = businessDependencies;
        }

        public ProductBrandListingWidgetController(IComponentPropertiesRetriever<ProductBrandWidgetProperties> propertiesRetriever,
                                        ICurrentPageRetriever currentPageRetriever) : base(propertiesRetriever, currentPageRetriever)
        {
        }

        public ActionResult Index()
        {
            var properties = GetProperties();

            ProductBrandViewModel model = new ProductBrandViewModel();

            model.ProductsBrands = _busDeps.ProductBrand.GetProductsBrands();
            model.CustomCssClasses = properties.CustomCssClasses;

            return PartialView("Widgets/_ProductBrandListingWidget", model);
        }
    }
}
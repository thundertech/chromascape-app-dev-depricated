﻿using Chromascape.Controllers.Widgets.ContentLeftImageRightHeroWidget;
using Chromascape.Models.Widgets.ContentLeftImageRightHeroWidget;
using CMS.DocumentEngine;
using Kentico.PageBuilder.Web.Mvc;
using System.Linq;
using System.Web.Mvc;

[assembly: RegisterWidget("Chromascape.Widgets.ContentLeftImageRightHero", typeof(ContentLeftImageRightHeroWidgetController), "Content Left Image Right Hero")]
namespace Chromascape.Controllers.Widgets.ContentLeftImageRightHeroWidget
{
    public class ContentLeftImageRightHeroWidgetController : WidgetController<ContentLeftImageRightHeroWidgetProperties>
    {
        public ActionResult Index()
        {
            var properties = GetProperties();
            var image = GetImage(properties);

            ContentLeftImageRightHeroViewModel model = new ContentLeftImageRightHeroViewModel();

            model.Image = image;
            model.Text = properties.Text;
            model.CustomCssClasses = properties.CustomCssClasses;

            return PartialView("Widgets/_ContentLeftImageRightHeroWidget", model);
        }

        protected DocumentAttachment GetImage(ContentLeftImageRightHeroWidgetProperties properties)
        {
            var page = GetPage();

            return page?.AllAttachments.FirstOrDefault(i => i.AttachmentGUID == properties.ImageGuid);
        }

    }
}
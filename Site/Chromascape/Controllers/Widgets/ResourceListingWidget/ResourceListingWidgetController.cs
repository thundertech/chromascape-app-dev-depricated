﻿using Business.DependencyInjection;
using Chromascape.Controllers.Widgets.ResourceListingWidget;
using Chromascape.Models.Widgets.ResourceListingWidget;
using Kentico.PageBuilder.Web.Mvc;
using System.Linq;
using System.Web.Mvc;


//TODO: fix or verify IconClass
[assembly: RegisterWidget("Chromascape.Widgets.ResourceListingWidget", typeof(ResourceListingWidgetController), "Resource Listing Widget", IconClass = "icon-cogwheel")]
namespace Chromascape.Controllers.Widgets.ResourceListingWidget
{
    public class ResourceListingWidgetController : WidgetController<ResourceListingWidgetProperties>
    {
        private IBusinessDependencies _busDeps { get; }

        public ResourceListingWidgetController(IBusinessDependencies businessDependencies)
        {
            _busDeps = businessDependencies;
        }

        public ActionResult Index()
        {
            ResourceListingViewModel model = new ResourceListingViewModel();
            var properties = GetProperties();

            model.Resources = _busDeps.ResourceItem.GetResources();
            model.CustomCssClasses = properties.CustomCssClasses;


            return PartialView("Widgets/_ResourceListingWidget", model);
        }


    }
}
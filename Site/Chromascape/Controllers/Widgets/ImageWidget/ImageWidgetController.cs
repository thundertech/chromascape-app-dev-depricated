﻿using Chromascape.Controllers.Widgets.ImageWidget;
using Chromascape.Models.Widgets.ImageWidget;
using CMS.DocumentEngine;
using Kentico.PageBuilder.Web.Mvc;
using System.Linq;
using System.Web.Mvc;

[assembly: RegisterWidget("Chromascape.Widgets.Image", typeof(ImageWidgetController), "Image", Description = "Image Widget", IconClass = "icon-picture")]
namespace Chromascape.Controllers.Widgets.ImageWidget
{
    public class ImageWidgetController : WidgetController<ImageWidgetProperties>
    {
        public ActionResult Index()
        {
            var properties = GetProperties();
            var image = GetImage(properties);

            ImageWidgetViewModel model = new ImageWidgetViewModel();

            model.Image = image;
            model.CustomCssClasses = properties.CustomCssClasses;

            return PartialView("Widgets/_ImageWidget", model);
        }

        protected DocumentAttachment GetImage(ImageWidgetProperties properties)
        {
            var page = GetPage();

            return page?.AllAttachments.FirstOrDefault(i => i.AttachmentGUID == properties.ImageGuid);
        }
    }
}
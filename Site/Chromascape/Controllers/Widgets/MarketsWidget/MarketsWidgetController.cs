﻿using Business.DependencyInjection;
using Chromascape.Controllers.Widgets.MarketsWidget;
using Chromascape.Models.Widgets.MarketsWidget;
using Kentico.PageBuilder.Web.Mvc;
using System.Web.Mvc;

[assembly: RegisterWidget("Chromascape.Widgets.MarketsWidget", typeof(MarketsWidgetController), "Markets Listing Widget")]
namespace Chromascape.Controllers.Widgets.MarketsWidget
{
    public class MarketsWidgetController : WidgetController<MarketsWidgetProperties>
    {
        IBusinessDependencies _busDeps { get; }

        public MarketsWidgetController(IBusinessDependencies businessDependencies)
        {
            _busDeps = businessDependencies;
        }

        public MarketsWidgetController(IComponentPropertiesRetriever<MarketsWidgetProperties> propertiesRetriever,
                                        ICurrentPageRetriever currentPageRetriever) : base(propertiesRetriever, currentPageRetriever)
        {
        }

        public ActionResult Index()
        {
            MarketsWidgetViewModel model = new MarketsWidgetViewModel();
            var properties = GetProperties();

            model.Markets = _busDeps.Markets.GetMarkets();

            foreach(var market in model.Markets)
                market.MarketLinks = _busDeps.Markets.GetMarketLinks(market.NodeAliasPath);

            model.CustomCssClasses = properties.CustomCssClasses;

            return PartialView("Widgets/_MarketsWidget", model);
        }
    }
}
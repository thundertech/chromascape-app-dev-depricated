﻿using Business.DependencyInjection;
using Chromascape.Controllers.Widgets.LinkListingWidget;
using Chromascape.Models.Widgets.LinkListingWidget;
using Kentico.PageBuilder.Web.Mvc;
using System.Linq;
using System.Web.Mvc;

[assembly: RegisterWidget("Chromascape.Widgets.LinkListing", typeof(LinkListingWidgetController), "Link Listing", IconClass = "icon-cogwheel")]
namespace Chromascape.Controllers.Widgets.LinkListingWidget
{
    public class LinkListingWidgetController : WidgetController<LinkListingWidgetProperties>
    {
        IBusinessDependencies _busDeps { get; }

        public LinkListingWidgetController(IBusinessDependencies businessDependencies)
        {
            _busDeps = businessDependencies;
        }

        public ActionResult Index()
        {
            LinkListingViewModel model = new LinkListingViewModel();
            var properties = GetProperties();

            if(properties.LinkGroupPath != null)
                model.Links = _busDeps.LinkItem.GetLinks(properties.LinkGroupPath.FirstOrDefault().NodeAliasPath);

            model.CustomCssClasses = properties.CustomCssClasses;

            return PartialView("Widgets/_LinkListingWidget", model);
        }
    }
}
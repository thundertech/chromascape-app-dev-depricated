﻿using Business.DependencyInjection;
using Chromascape.Controllers.Widgets.LeadershipListingWidget;
using Chromascape.Models.Widgets.LeadershipListingWidget;
using Kentico.PageBuilder.Web.Mvc;
using System.Web.Mvc;

[assembly: RegisterWidget("Chromascape.Widgets.LeadershipListingWidget", typeof(LeadershipController), "Leadership Listing Widget", IconClass = "icon-users")]
namespace Chromascape.Controllers.Widgets.LeadershipListingWidget
{
    public class LeadershipController : WidgetController<LeadershipListingProperties>
    {
        private IBusinessDependencies _busDeps { get; }

        public LeadershipController(IBusinessDependencies businessDependencies)
        {
            _busDeps = businessDependencies;
        }

        public ActionResult Index()
        {
            LeadershipListingViewModel model = new LeadershipListingViewModel();
            var properties = GetProperties();

            model.LeadershipMembers = _busDeps.Leadership.GetLeadershipTeam();
            model.CustomCssClasses = properties.CustomCssClasses;

            return PartialView("Widgets/_LeadershipListingWidget", model);
        }
    }
}
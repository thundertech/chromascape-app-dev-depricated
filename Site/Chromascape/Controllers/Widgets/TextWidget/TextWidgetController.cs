﻿using Chromascape.Controllers.Widgets.TextWidget;
using Chromascape.Models.Widgets.TextWidget;
using Kentico.PageBuilder.Web.Mvc;
using System.Web.Mvc;

[assembly: RegisterWidget("Chromascape.Widgets.TextWidget", typeof(TextWidgetController), "Text Block", IconClass = "icon-l-text")]
namespace Chromascape.Controllers.Widgets.TextWidget
{
    public class TextWidgetController : WidgetController<TextWidgetProperties>
    {
        public TextWidgetController()
        {

        }

        public TextWidgetController(IComponentPropertiesRetriever<TextWidgetProperties> propertiesRetriever,
                                        ICurrentPageRetriever currentPageRetriever) : base(propertiesRetriever, currentPageRetriever)
        {
        }

        // GET: TextWidget
        public ActionResult Index()
        {
            var properties = GetProperties();

            

            return PartialView("Widgets/_TextWidget", new TextWidgetViewModel { Text = properties.Text, CustomCssClasses = properties.CustomCssClasses });
        }
    }
}
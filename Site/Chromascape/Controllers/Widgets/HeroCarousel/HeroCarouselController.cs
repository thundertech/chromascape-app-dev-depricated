﻿using Business.DependencyInjection;
using Chromascape.Controllers.Widgets.HeroCarousel;
using Chromascape.Models.Widgets.HeroCarousel;
using CMS.EventLog;
using Kentico.PageBuilder.Web.Mvc;
using System;
using System.Linq;
using System.Web.Mvc;

[assembly: RegisterWidget("ChromaScape.Widgets.HeroCarousel", typeof(HeroCarouselController), "Hero Carousel")]

namespace Chromascape.Controllers.Widgets.HeroCarousel
{
    public class HeroCarouselController : WidgetController<HeroCarouselProperties>
    {
        private IBusinessDependencies busDeps { get; }

        public HeroCarouselController(IBusinessDependencies businessDependencies)
        {
            busDeps = businessDependencies;
        }

        public HeroCarouselController(IComponentPropertiesRetriever<HeroCarouselProperties> propertiesRetriever,
                                        ICurrentPageRetriever currentPageRetriever) : base(propertiesRetriever, currentPageRetriever)
        {

        }

        public ActionResult Index()
        {
            try
            {
                HeroCarouselViewModel model = new HeroCarouselViewModel();
                var properties = GetProperties();

                var parentNodeAlias = properties.HeroCarouselFolderPath.FirstOrDefault().NodeAliasPath;

                model.Slides = busDeps.HeroCarousel.GetSlides(parentNodeAlias);
                model.CustomCssClasses = properties.CustomCssClasses;
                
                return PartialView("Widgets/_HeroCarousel", model);
            }
            catch (Exception ex)
            {
                EventLogProvider.LogException("Hero Carousel", "ERROR", ex);
                return null;
            }
        }
    }
}
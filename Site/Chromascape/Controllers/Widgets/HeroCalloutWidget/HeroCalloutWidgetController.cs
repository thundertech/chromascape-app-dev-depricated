﻿using Business.Interfaces;
using Chromascape.Controllers.Widgets.HeroCalloutWidget;
using Chromascape.Models.Widgets.HeroCalloutWidget;
using Kentico.PageBuilder.Web.Mvc;
using System.Web.Mvc;

[assembly: RegisterWidget("Chromascape.Widgets.HeroCallout", typeof(HeroCalloutWidgetController), "Hero Callout")]
namespace Chromascape.Controllers.Widgets.HeroCalloutWidget
{
    public class HeroCalloutWidgetController : WidgetController<HeroCalloutWidgetProperties>
    {

        IHeroCallout HeroCalloutRepository { get; }

        public HeroCalloutWidgetController(IHeroCallout heroCallout)
        {
            HeroCalloutRepository = heroCallout;
        }

        public HeroCalloutWidgetController(IComponentPropertiesRetriever<HeroCalloutWidgetProperties> propertiesRetriever,
                                        ICurrentPageRetriever currentPageRetriever) : base(propertiesRetriever, currentPageRetriever)
        {

        }
        
        public ActionResult Index()
        {
            var currentPage = GetPage();
            var properties = GetProperties();

            HeroCalloutWidgetViewModel model = new HeroCalloutWidgetViewModel();

            model.HeroCallout = HeroCalloutRepository.GetHeroCallout(currentPage.NodeAliasPath);
            model.CustomCssClasses = properties.CustomCssClasses;

            return PartialView("Widgets/_HeroCalloutWidget", model);
        }
    }
}
﻿using CMS.DocumentEngine;
using CMS.SiteProvider;
using Kentico.Content.Web.Mvc;
using Kentico.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace Chromascape.Utils
{
    public class NodeAliasPathConstraint : IRouteConstraint
    {
        private readonly string nodeClassName;

        public NodeAliasPathConstraint(string nodeClassName) =>
            this.nodeClassName = nodeClassName;

        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            if(!values.TryGetValue(parameterName, out object nodeAliasPathObj))
            {
                return false;
            }

            string nodeAliasPath = $"/{nodeAliasPathObj as string}";

            bool isPreview = HttpContext.Current.Kentico().Preview().Enabled;

            var node = DocumentHelper.GetDocuments()
                .WhereEquals(nameof(TreeNode.NodeAliasPath), nodeAliasPath)
                .LatestVersion(isPreview)
                .Published(!isPreview)
                .OnSite(SiteContext.CurrentSite.SiteName)
                .CombineWithDefaultCulture()
                .TopN(1)
                .Column(nameof(TreeNode.ClassName))
                .FirstOrDefault();

            if (node is null)
            {
                return false;
            }

            if (string.Equals(node.ClassName, nodeClassName, StringComparison.OrdinalIgnoreCase))
            {
                // ❗ We replace the `nodeAliasPath` route value with the one we prefixed
                // above so that the ArticlesController.Show(string nodeAliasPath)
                // call is passed a correct TreeNode.NodeAliasPath value

                values[parameterName] = nodeAliasPath;

                return true;
            }
            
            return true;
        }
    }
}
﻿using System;

namespace Chromascape.Utils
{
    public class PageTypeRouteAttribute : Attribute
    {
        public PageTypeRouteAttribute(params string[] classNames) =>
            ClassNames = classNames;

        public string[] ClassNames { get; }
    }
}
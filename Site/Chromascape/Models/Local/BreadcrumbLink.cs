﻿namespace Chromascape.Models.Local
{
    public class BreadcrumbLink
    {
        public string Name { get; set; }

        public string Url { get; set; }

        public bool IsActive { get; set; }
    }
}
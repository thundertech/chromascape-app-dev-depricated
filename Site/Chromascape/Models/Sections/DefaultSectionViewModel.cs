﻿namespace Chromascape.Models.Sections
{
    public class DefaultSectionViewModel
    {
        public string BackgroundColor { get; set; }

        public bool AddTopPadding { get; set; }

        public bool AddBottomPadding { get; set; }

        public string CustomCssClasses { get; set; }
    }
}
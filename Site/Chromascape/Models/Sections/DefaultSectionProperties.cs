﻿using Kentico.Forms.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;

namespace Chromascape.Models.Sections
{
    public class DefaultSectionProperties : ISectionProperties
    {
        [EditingComponent(DropDownComponent.IDENTIFIER, Label = "Select section background color", Order = 0)]
        [EditingComponentProperty(nameof(DropDownProperties.DataSource), ";Default - White\r\nbg-light-blue;Light Blue;\r\nbg-light-gray;Light Gray")]
        public string BackgroundColor { get; set; }

        [EditingComponent(CheckBoxComponent.IDENTIFIER, Label = "Add top padding", Order = 1)]
        public bool AddPaddingTop { get; set; } = true;

        [EditingComponent(CheckBoxComponent.IDENTIFIER, Label = "Add bottom padding", Order = 2)]
        public bool AddPaddingBottom { get; set; } = true;

        [EditingComponent(TextInputComponent.IDENTIFIER, Label = "Enter custom CSS classes (separate by a space)")]
        public string CustomCssClasses { get; set; }
    }
}
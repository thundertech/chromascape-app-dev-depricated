﻿using Business.Dto.Menus;
using Business.Dto.PageMetaData;
using Business.Dto.SiteFooter;
using Business.Dto.SiteHeader;
using Business.Dto.SocialLinks;
using Chromascape.Models.EmailMarketing;
using Chromascape.Models.Local;
using Chromascape.Models.Search;
using System.Collections.Generic;

namespace Chromascape.Models
{
    public class PageViewModel : IViewModel
    {
        public PageViewModel()
        {
            Metadata = new PageMetaDataDto();
            SocialLinks = new List<SocialLinksDto>();
            SiteFooter = new SiteFooterDto();
            SiteHeader = new SiteHeaderDto();
            MainMenuFolder = new MenuFolderDto();
            FooterMenuFolder = new MenuFolderDto();
            Breadcrumbs = new List<BreadcrumbLink>();
            SearchVM = new SearchViewModel();
            UnsubscribeVM = new MarketingEmailUnsubscribeModel();
        }

        public PageMetaDataDto Metadata { get; set; }

        public List<SocialLinksDto> SocialLinks { get; set; }

        public SiteFooterDto SiteFooter { get; set; }

        public SiteHeaderDto SiteHeader { get; set; }

        public MenuFolderDto MainMenuFolder { get; set; }

        public MenuFolderDto FooterMenuFolder { get; set; }

        public bool ShowBreadcrumbs { get; set; } = true;

        public List<BreadcrumbLink> Breadcrumbs { get; set; }

        public string CurrentPageName { get; set; }

        public SearchViewModel SearchVM { get; set; }

        public MarketingEmailUnsubscribeModel UnsubscribeVM { get; set; }
    }

    /// <summary>
    /// Inherits the base class and accepts generic view model which represents data for other pages on the website
    /// </summary>
    /// <typeparam name="TViewModel"></typeparam>
    public class PageViewModel<TViewModel> : PageViewModel where TViewModel : IViewModel
    {
        public TViewModel Data { get; set; }
    }
}
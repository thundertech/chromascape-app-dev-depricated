﻿using Kentico.Forms.Web.Mvc;

namespace Chromascape.Models.Widgets
{
    public class WidgetBaseProperties
    {
        [EditingComponent(TextInputComponent.IDENTIFIER, Label = "Enter custom CSS classes (separate by a space)")]
        public string CustomCssClasses { get; set; }

    }
}
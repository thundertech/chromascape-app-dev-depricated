﻿using Business.Dto.ProductBrand;
using System.Collections.Generic;

namespace Chromascape.Models.Widgets.ProductBrandWidget
{
    public class ProductBrandViewModel : WidgetBaseViewModel
    {
        public ProductBrandViewModel()
        {
            ProductsBrands = new List<ProductBrandDto>();
        }

        public List<ProductBrandDto> ProductsBrands { get; set; }
    }
}
﻿using CMS.DocumentEngine;

namespace Chromascape.Models.Widgets.ImageWidget
{
    public class ImageWidgetViewModel : WidgetBaseViewModel
    {
        public DocumentAttachment Image { get; set; }
    }
}
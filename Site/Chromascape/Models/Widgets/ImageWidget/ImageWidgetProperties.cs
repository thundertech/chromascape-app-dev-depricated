﻿using Kentico.PageBuilder.Web.Mvc;
using System;

namespace Chromascape.Models.Widgets.ImageWidget
{
    public class ImageWidgetProperties : WidgetBaseProperties, IWidgetProperties
    {
        public Guid ImageGuid { get; set; }

    }
}
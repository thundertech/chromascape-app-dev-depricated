﻿using Business.Dto.Leadership;
using System.Collections.Generic;

namespace Chromascape.Models.Widgets.LeadershipListingWidget
{
    public class LeadershipListingViewModel : WidgetBaseViewModel
    {
        public List<LeadershipMemberDto> LeadershipMembers { get; set; }
    }
}
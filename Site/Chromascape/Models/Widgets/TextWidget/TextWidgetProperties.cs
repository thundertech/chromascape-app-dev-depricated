﻿using Kentico.PageBuilder.Web.Mvc;

namespace Chromascape.Models.Widgets.TextWidget
{
    public class TextWidgetProperties : WidgetBaseProperties, IWidgetProperties
    {
        /// <summary>
        /// HTML formatted text
        /// </summary>
        /// 
        public string Text { get; set; }
    }
}
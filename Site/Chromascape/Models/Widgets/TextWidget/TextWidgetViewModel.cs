﻿namespace Chromascape.Models.Widgets.TextWidget
{
    public class TextWidgetViewModel : WidgetBaseViewModel
    {
        public string Text { get; set; }
    }
}
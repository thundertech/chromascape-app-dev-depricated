﻿using CMS.DocumentEngine;

namespace Chromascape.Models.Widgets.ContentLeftImageRightHeroWidget
{
    public class ContentLeftImageRightHeroViewModel : WidgetBaseViewModel
    {
        public string Text { get; set; }

        public DocumentAttachment Image { get; set; }
    }
}
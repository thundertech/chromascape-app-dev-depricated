﻿using Kentico.PageBuilder.Web.Mvc;
using System;

namespace Chromascape.Models.Widgets.ContentLeftImageRightHeroWidget
{
    public class ContentLeftImageRightHeroWidgetProperties : WidgetBaseProperties, IWidgetProperties
    {
        public Guid ImageGuid { get; set; }

        public string Text { get; set; }
    }
}
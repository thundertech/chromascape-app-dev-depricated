﻿using Business.Dto.Markets;
using CMS.DocumentEngine;
using System.Collections.Generic;

namespace Chromascape.Models.Widgets.MarketsWidget
{
    public class MarketsWidgetViewModel : WidgetBaseViewModel
    {
        public MarketsWidgetViewModel()
        {
            Markets = new List<MarketDto>();
        }

        /// <summary>
        /// Text
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Background Image
        /// </summary>
        public DocumentAttachment Image { get; set; }

        public List<MarketDto> Markets { get; set; }
    }
}
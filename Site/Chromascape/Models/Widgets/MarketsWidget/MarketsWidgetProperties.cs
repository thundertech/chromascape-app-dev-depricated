﻿using Kentico.PageBuilder.Web.Mvc;
using System;

namespace Chromascape.Models.Widgets.MarketsWidget
{
    public class MarketsWidgetProperties : WidgetBaseProperties, IWidgetProperties
    {
        /// <summary>
        /// Guid of the background image
        /// </summary>
        public Guid ImageGuid { get; set; }

        /// <summary>
        /// Text in the opposite side from the image
        /// </summary>
        public string Text { get; set; }
    }
}
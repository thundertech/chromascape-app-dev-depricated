﻿using Business.Dto.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chromascape.Models.Widgets.ResourceListingWidget
{
    public class ResourceListingViewModel : WidgetBaseViewModel
    {
        //TODO: lifted from LinkListing, Leadership does not have a setter

        public ResourceListingViewModel()
        {
            Resources = new List<ResourceItemDto>();
        }


        public List<ResourceItemDto> Resources { get; set; }
    }
}
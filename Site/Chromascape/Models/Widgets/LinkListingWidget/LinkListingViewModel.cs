﻿using Business.Dto.LinkListing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Chromascape.Models.Widgets.LinkListingWidget
{
    public class LinkListingViewModel : WidgetBaseViewModel
    {
        public LinkListingViewModel()
        {
            Links = new List<LinkItemDto>();
        }

        public List<LinkItemDto> Links { get; set; }
    }
}
﻿using Kentico.Components.Web.Mvc.FormComponents;
using Kentico.Forms.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;
using System.Collections.Generic;

namespace Chromascape.Models.Widgets.LinkListingWidget
{
    public class LinkListingWidgetProperties : WidgetBaseProperties, IWidgetProperties
    {
        [EditingComponent(PathSelector.IDENTIFIER)]
        [EditingComponentProperty(nameof(PathSelectorProperties.Label), "Select the Link Group")]
        public List<PathSelectorItem> LinkGroupPath { get; set; }

    }
}
﻿using Kentico.Components.Web.Mvc.FormComponents;
using Kentico.Forms.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;
using System.Collections.Generic;

namespace Chromascape.Models.Widgets.HeroCarousel
{
    public class HeroCarouselProperties : WidgetBaseProperties, IWidgetProperties
    {
        /// <summary>
        /// Path to the Hero Carousel Folder that holds the Hero Carousel Slides
        /// </summary>
        [EditingComponent(PathSelector.IDENTIFIER)]
        [EditingComponentProperty(nameof(PathSelectorProperties.Label), "Select Hero Carousel Folder")]
        public List<PathSelectorItem> HeroCarouselFolderPath { get; set; }

    }
}
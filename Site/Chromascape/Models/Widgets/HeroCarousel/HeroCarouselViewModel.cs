﻿using Business.Dto.HeroCarousel;
using System.Collections.Generic;

namespace Chromascape.Models.Widgets.HeroCarousel
{
    public class HeroCarouselViewModel : WidgetBaseViewModel
    {
        public HeroCarouselViewModel()
        {
            Slides = new List<HeroCarouselSlideDto>();
        }

        public List<HeroCarouselSlideDto> Slides { get; set; }
    }
}
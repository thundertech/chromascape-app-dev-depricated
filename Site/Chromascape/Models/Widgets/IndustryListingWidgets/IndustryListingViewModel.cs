﻿using Business.Dto.Industries;
using System.Collections.Generic;

namespace Chromascape.Models.Widgets.IndustryListingWidgets
{
    public class IndustryListingViewModel : WidgetBaseViewModel
    {
        public IndustryListingViewModel()
        {
            Industries = new List<IndustryDto>();
        }

        public List<IndustryDto> Industries { get; set; }

        public string IndustryCategoryPath { get; set; }
    }
}
﻿using Kentico.Components.Web.Mvc.FormComponents;
using Kentico.Forms.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;
using System.Collections.Generic;

namespace Chromascape.Models.Widgets.IndustryListingWidgets
{
    public class IndustryListingWidgetProperties : WidgetBaseProperties, IWidgetProperties
    {
        /// <summary>
        /// Path to the Industry Category container / folder containing the industries to be listed
        /// </summary>
        /// 
        [EditingComponent(PathSelector.IDENTIFIER)]
        [EditingComponentProperty(nameof(PathSelectorProperties.Label), "Select the Industry Category")]
        public List<PathSelectorItem> IndustryCategoryPath { get; set; }
    }
}
﻿using Business.Dto.HeroCallout;

namespace Chromascape.Models.Widgets.HeroCalloutWidget
{
    public class HeroCalloutWidgetViewModel : WidgetBaseViewModel
    {
        public HeroCalloutWidgetViewModel()
        {
            HeroCallout = new HeroCalloutDto();
        }

        public HeroCalloutDto HeroCallout { get; set; }
    }
}
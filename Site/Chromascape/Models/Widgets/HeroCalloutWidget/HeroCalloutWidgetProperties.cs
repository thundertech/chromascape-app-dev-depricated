﻿using Kentico.PageBuilder.Web.Mvc;

namespace Chromascape.Models.Widgets.HeroCalloutWidget
{
    public class HeroCalloutWidgetProperties : WidgetBaseProperties, IWidgetProperties
    {
        /// <summary>
        /// Button URL
        /// </summary>
        public string ButtonUrl { get; set; }

    }
}
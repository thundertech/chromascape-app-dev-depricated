﻿namespace Chromascape.Models.InlineEditors.ImageUploaderEditor
{
    public enum PanelPositionEnum
    {
        /// <summary>
        /// Aligned to the top of the widget content.
        /// </summary>
        Top,


        /// <summary>
        /// Aligned to the center of the widget content.
        /// </summary>
        Center,


        /// <summary>
        /// Aligned to the bottom of the widget content.
        /// </summary>
        Bottom,

        /// <summary>
        /// Aligned to the right of the widget content.
        /// </summary>
        Right,

        /// <summary>
        /// Aligned to the left of the widget content.
        /// </summary>
        Left
    }
}
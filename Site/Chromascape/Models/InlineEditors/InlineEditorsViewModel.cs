﻿namespace Chromascape.Models.InlineEditors
{
    public abstract class InlineEditorsViewModel
    {
        public string PropertyName { get; set; }
    }
}
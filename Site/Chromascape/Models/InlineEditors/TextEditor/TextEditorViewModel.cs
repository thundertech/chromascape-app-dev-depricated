﻿namespace Chromascape.Models.InlineEditors.TextEditor
{
    public class TextEditorViewModel : InlineEditorsViewModel
    {
        /// <summary>
        /// HTML formatted text.
        /// </summary>
        public string Text { get; set; }


        /// <summary>
        /// Indicates if the formatting is enabled for the editor.
        /// </summary>
        public bool EnableFormatting { get; set; } = true;
    }
}
﻿using CMS.Search;
using System.Collections.Generic;

namespace Chromascape.Models.Search
{
    public class SearchViewModel
    {
        public SearchViewModel()
        {
            Items = new List<SearchResultItem>();
        }

        public string Query { get; set; }

        public List<SearchResultItem> Items { get; set; }

    }
}
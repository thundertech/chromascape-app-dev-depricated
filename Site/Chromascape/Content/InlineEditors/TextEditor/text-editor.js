﻿(function () {
    window.kentico.pageBuilder.registerInlineEditor("text-editor", {
        init: function (options) {
            var editor = options.editor;

            MediumButton = window['MediumButton'];

            var config = {
                toolbar: {
                    buttons: [
                        'bold',
                        'italic',
                        'underline',
                        'orderedlist',
                        'unorderedlist',
                        'h1',
                        'h2',
                        'h3',
                        'h4',
                        'justifyLeft',
                        'justifyCenter',
                        'justifyRight',
                        'superscript',
                        'cran',
                        'mar',
                        'sg',
                        'blue',
                        'anchor']
                },
                anchor: {
                    customClassOption: null,
                    customClassOptionText: 'Button',
                    linkValidation: false,
                    placeholderText: 'Paste or type a link',
                    targetCheckbox: true,
                    targetCheckboxText: 'Open in new window'
                },
                imageDragging: false,
                extensions: {
                    imageDragging: {},
                    cran: new MediumButton({
                        label: '<b>Cran</b>',
                        start: '<span class="text-cranberry">',
                        end: '</span>'
                    }),
                    mar: new MediumButton({
                        label: '<b>Mar</b>',
                        start: '<span class="text-marigold">',
                        end: '</span>'
                    }),
                    sg: new MediumButton({
                        label: '<b>SG</b>',
                        start: '<span class="text-seagreen">',
                        end: '</span>'
                    }),
                    blue: new MediumButton({
                        label: '<b>Blue</b>',
                        start: '<span class="text-admiral-blue">',
                        end: '</span>'
                    })
                }
            };

            if (editor.dataset.enableFormatting === "False") {
                config.toolbar = false;
                config.keyboardCommands = false;
            }

            MediumEditor.extensions.form.prototype.formSaveLabel = "<span>Ok</span>";
            MediumEditor.extensions.form.prototype.formCloseLabel = "<span>Cancel</span>";

            var mediumEditor = new MediumEditor(editor, config);

            mediumEditor.subscribe("editableInput", function () {
                var event = new CustomEvent("updateProperty", {
                    detail: {
                        name: options.propertyName,
                        value: mediumEditor.getContent(),
                        refreshMarkup: false
                    }
                });

                editor.dispatchEvent(event);
            });
        },

        destroy: function (options) {
            var mediumEditor = MediumEditor.getEditorFromElement(options.editor);
            if (mediumEditor) {
                mediumEditor.destroy();
            }
        },

        dragStart: function (options) {
            var mediumEditor = MediumEditor.getEditorFromElement(options.editor);
            var focusedElement = mediumEditor && mediumEditor.getFocusedElement();

            var focusedMediumEditor = focusedElement && MediumEditor.getEditorFromElement(focusedElement);
            var toolbar = focusedMediumEditor && focusedMediumEditor.getExtensionByName("toolbar");

            if (focusedElement && toolbar) {
                toolbar.hideToolbar();
                focusedElement.removeAttribute("data-medium-focused");
            }
        }
    });
})();

using System.Reflection;
using System.Runtime.InteropServices;

using CMS;

[assembly: AssemblyDiscoverable]

[assembly: AssemblyTitle("Chromascape")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("3819ffb3-95cc-4603-8bd9-b978a2d301b4")]

[assembly: AssemblyProduct("Chromascape")]
[assembly: AssemblyCopyright("© 2019")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("12.0.0.0")]
[assembly: AssemblyFileVersion("12.0.6900.17470")]
[assembly: AssemblyInformationalVersion("12.0.0")]
